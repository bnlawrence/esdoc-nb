# README #

This is a simulation notebook tool, aimed at those who plan and carry out earth system simulations.  It is at a very early stage of development. You shouldn't use it for anything real to we get at least to v0.1!

### Quick Summary ###

The tool provides a paned notebook which allows the user to create, view, and edit items in lists of experiment and simulation descriptions (and associated documents). The underlying system exploits the esdoc formalism, and will eventually support the export of documents into the esdoc ecosystem.

### How do I get set up? ###

The tool is written using pygtk. 

At the moment you need to either: 

* checkout the repo and change directory to the esdoc-nb directory and use your favourite python to execute simpleRun.py
* checkout the repo and easy_install the package.

Note the dependency on pygraphviz.

### Contribution guidelines ###

If you can provide features or bug fixes, please do, if possible with a unit test showing the feature (recognising that's not easy with GUIs, but if you can do.)

### Who do I talk to? ###

Bryan Lawrence, but don't expect a quick turn around, this is an activity that fits around his day job.