from esdoc_nb.mp.core.lib.factory import cimFactory
from esdoc_nb.mp.scripts import CMIP6_DOCS
from esdoc_nb.mp.scripts.extended_vocab_view import VocabView
from esdoc_nb.mp.umlview import ClassDoc


def domain_view(directory):
    """ This produces a complete view for each domain """
    domains = ['Ocean']
    for d in domains:
        v = VocabView('cmip6', d, 1)
        v.plot(file_base=directory, fmt='png')


    # TODO: Ensure constraints are properly supported.
    # Build a [d] instance, and walk the properties to build diagrams
    # for each property that isn't pythonic ...

    oceanset = []


    for p in oceanset:
        v = VocabView('cmip6',p)
        v.plot(file_base='%s/ocean/' % CMIP6_DOCS, fmt='pdf', orientation='LR')

def process_view(directory):
    """ Here, an example for looking at just one process"""
    v = VocabView('cmip6', 'atmos_radiation')
    v.plot(file_base=directory, fmt='png')

def radiation_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('LwProperties', factory=factory)
    d.plot(filebase='%s/doc_radiation' % directory, dpi=300, fmt='png')

def ocean_enum_view(directory):
    """ And here, an example for just one class """
    factory = cimFactory()
    factory.add_extension_package('cmip6')
    d = ClassDoc('OceanTimesteppingProps', factory=factory)
    d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')
    #d = ClassDoc('OceanTimesteppingTra', factory=factory)
    #d.plot(filebase='%s/ocean_enums' % directory, dpi=300, fmt='png')


if __name__ == "__main__":

    domain_view('%s/vocabs/' % CMIP6_DOCS)
    #radiation_view(CMIP6_DOCS)
    #process_view('test_')
    #ocean_enum_view('%s/ocean/' % CMIP6_DOCS)
