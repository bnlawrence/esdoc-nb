import unittest

from esdoc_nb.mp.core.lib.meta import decode_extensions_syntax2, checkmetamodel, checkenums
from esdoc_nb.mp.core.lib.factory import cimFactory

#from esdoc_nb.mp.core.lib.meta import decode_extensions

#
# This is a set of tests for an initial attempt to read and parse the new "Hassell/Guilyardi" extended
# vocabulary syntax.
#

class TestSyntax2(unittest.TestCase):

    def setUp(self):
        print "self.constructors = decode_extensions_syntax2('cmip6') :::::"
        names, self.constructors = decode_extensions_syntax2('cmip6')
#        print "self.constructors = decode_extensions('cmip6') :::::"
#        self.constructors = decode_extensions('cmip6')

        print '\nself.constructors =',self.constructors,'\n'

    def test_read(self):
        """ test reading syntax"""
        print 'constructors.keys() =',self.constructors.keys()


    def test_factory(self):
        """ test the extensions work in the factory and base classes are available """
        self.factory = cimFactory()
        self.factory.add_extension_package('cmip6')
        for c in self.constructors:
            constructor = self.constructors[c]
            if self.constructors[c]['type'] != 'enum':
                ok = checkmetamodel(constructor, self.factory.fullset)
                bh = self.factory.find_base_classes(c)
            else:
                ok = checkenums(constructor)






if __name__=="__main__":
    unittest.main()

