#
# This simple script generates uml class diagrams as separate diagrams
# for all CIM2 classes
#
from esdoc_nb.mp.umlview import umlDiagram, ClassDoc
from esdoc_nb.mp.scripts import CIM_DOCS

if __name__ == "__main__":
    d = umlDiagram()
    for k in d.factory.enums:
        try:
            print 'Plotting ',k
            d.setEnumDoc(k)
            d.plot(filebase='%s/enum_docs/%s' % (CIM_DOCS, k))
        except:
            print 'Error for %s' % k
            raise