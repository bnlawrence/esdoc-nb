from esdoc_nb.mp.umlview import umlDiagram, PackageDiagram
from esdoc_nb.mp.scripts import CIM_DOCS


def makeEm():
    """ Make those package diagrams """

    d = umlDiagram()
    for p in d.factory.packages:
        d.setPackage(p)
        d.setAssociationEdges()
        d.plot(filebase='%s/package_%s' % (CIM_DOCS, p), title='cim2 %s package'%p, fmt='pdf')
        d.plot(filebase='%s/package_%s_short' % (CIM_DOCS, p), title='cim2 %s package'%p, fmt='pdf', name_only=True)

    d = PackageDiagram(icondir='../icons/')
    d.plot(filebase='%s/cim2_packages' % CIM_DOCS, layout='dot', fmt='pdf')



def makespecials():
    """ Makes important subset documentation """

    wanted = ['Ensemble','Simulation','EnsembleAxis','EnsembleMember',
              'AxisMember', 'EnsembleTypes','Conformance']
    d = umlDiagram()
    d.setClasses(wanted)
    d.setAssociationEdges()
    d.plot(filebase='%s/ensemble_documentation' % CIM_DOCS, title='CMIP6 Ensemble Perspective')

    wanted = ['Ensemble','Model','Conformance', 'Simulation']
    d.setClasses(wanted)
    d.setAssociationEdges()
    d.plot(filebase='%s/modelling_documentation' % CIM_DOCS, title='Modelling Perspective')

    wanted = ['Simulation', 'Downscaling']
    d.setClasses(wanted)
    d.setAssociationEdges()
    d.plot(filebase='%s/downscaling_docs' % CIM_DOCS, title='Downscaling Perspective')

    wanted = ['Model','SoftwareComponent','ScientificDomain','DevelopmentPath']
    d.setClasses(wanted)
    d.setAssociationEdges()
    d.plot(filebase='%s/software_docs' % CIM_DOCS, title='Software Perspective')

def make_perspectives():
    """ Makes perspective diagrams, used in email conversations
    """
    d = umlDiagram()
    d.setPerspective('Simulation')
    d.setAssociationEdges()
    d.plot(filebase='%s/email_simulation_docs' % CIM_DOCS, title='Simulation Perspective')

    d = umlDiagram()
    d.setPerspective('Dataset')
    d.setAssociationEdges()
    d.plot(filebase='%s/email_data_docs' % CIM_DOCS, title='Dataset Perspective')


def makeSimulation4David():
    """ Creates the uml diagram necessary for understanding key details of simulation handling"""
    d = umlDiagram()
    d.setClasses(['Simulation','EnsembleMember'])
    d.setAssociationEdges()
    d.plot(filebase='%s/simulation_cmip6' % CIM_DOCS, title='Simulation Classes')

def make_paper_docs():
    """ Makes specific plots for the CMIP6 documentation paper"""
    d = umlDiagram()
    d.setClasses(['Simulation','Ensemble','EnsembleAxis','EnsembleMember','UberEnsemble','AxisMember'])
    d.setAssociationEdges()
    d.plot(filebase='%s/cmip6_docs_simulation' % CIM_DOCS, title='Simulation',dpi=300, layout='dot', fmt='pdf')

def make_alldocs_plot():
    """ Makes a plot with all the classes which carry the document stereotype"""
    d = umlDiagram()
    classes = d.factory.classes
    toplot = []
    for c in classes:
        if d.factory.isdoc(c):# and not d.factory.isabstract(c):
            toplot.append(c)
    d.setClasses(toplot, show_base_classes=False)
    d.setAssociationEdges()
    d.plot(filebase='%s/cmip6_docs_documents' % CIM_DOCS, title='Documents and their relationships', dpi=300,
           layout='dot', name_only=True, show_base_classes=False, fmt='pdf')

def make_coredocs():
    """ Cutdown version of make_alldocs to limit to cmip6 things of significant interest"""
    d = umlDiagram()
    classes = d.factory.classes
    toplot = []
    for c in classes:
        if d.factory.isdoc(c):# and not d.factory.isabstract(c):
            toplot.append(c)
    # now remove some specific ones to make the picture a bit cleaner
    for c in ['SimulationPlan','Downscaling',
              'EnsembleRequirement', 'MultiEnsemble', 'TemporalConstraint',
              'DomainProperties', 'MultiTimeEnsemble', 'OutputTemporalRequirement',
              'ForcingConstraint']:
        toplot.remove(c)
    d.setClasses(toplot, show_base_classes=False)
    d.setAssociationEdges()
    d.plot(filebase='%s/cmip6_docs_coredocs' % CIM_DOCS, dpi=300,
           layout='dot', name_only=True, show_base_classes=False, fmt='pdf')



if __name__=="__main__":
    #make_perspectives()
    #make_paper_docs()
    #make_alldocs_plot()
    #make_coredocs()
    makeEm()
    exit()
    makespecials()
    makeSimulation4David()
