import pygraphviz as pgv

from esdoc_nb.mp.core.lib.meta import PropertyType
from esdoc_nb.mp.core.lib.factory import cimFactory
from esdoc_nb.mp.umlview import PackageColour

def make_picker(factory):
    """ Make a colour picker to shade the nodes using superclasses"""
    packages = {'classes':factory.classes.keys(), 'enums':factory.enums.keys()}
    picker = PackageColour(packages)
    return picker

class VocabView(object):

    """ Provides a UML -instance-like view of the extension vocabularies. We need to do something different
    because the relationships are via values which only show up in instances of the constrained classes.
    """
    def __init__(self, package, root, link_depth = None):
        """ Plot a package ...starting from root ... if you want to control the number of links
        use link_depth = number_of_links """

        self.factory = cimFactory()
        self.package = package
        self.factory.add_extension_package(package)

        if link_depth:
            assert isinstance(link_depth, int) and link_depth > 0, \
                "Invalid value for link depth - %s " % link_depth

        self.orientation=''

        self.constructors = {}
        self.vocabs = {}

        self.constructors = self.factory.fullset
        self.vocabs = self.factory.enums

        self.package = package
        self.root = root
        self.nodes = [root]
        self.edges = []
        self._add2_graph_from(root, link_depth)

    def _add2_graph_from(self, node, link_depth = None):
        """ Find the nodes and edges starting from node, if
         link_depth is present and greater than zero, limit the link depth
         to that value."""

        assert node in self.constructors, 'No constructor available for %s' % node
        constructor = self.constructors[node]

        if 'properties' in constructor:
            properties = [p for p in constructor['properties'] if not p[1].pythonic]
        else:
            properties = []

        if 'constraints' in constructor:
            for c in constructor['constraints']:
                c_type = c[0]
                link = c[1]
                targets = c[2]
                assert c_type in ['constant',], "VocabView doesn't yet understand constraint type %s " % c_type
                if not isinstance(targets, list):
                    targets = [targets,]
                ntargets = [t for t in targets if t in self.factory.fullset]
                properties.append((link, ntargets))

        for p in properties:
            if not isinstance(p[1], list):
                targets = [p[1],]
            else:
                targets = p[1]
            link = p[0]
            for target in targets:
                if isinstance(target,PropertyType):
                    target = target.camel
                edge = (node, target, link)
                if edge not in self.edges:
                    self.edges.append(edge)
                if target not in self.nodes:
                    self.nodes.append(target)
                    if target not in self.vocabs:
                        if link_depth:
                            depth = link_depth - 1
                            if depth > 0:
                                self._add2_graph_from(target, depth)
                        else:
                            self._add2_graph_from(target)


    def plot(self, file_base='', fmt='pdf',  orientation='TB', dpi=150, layout='dot', coloured=True,  key=True,
            **kw):
        """ Plot... if you want to control the orientiation which is
        by default TB = 'top to bottom', choose orientation LR = 'left to right """

        assert orientation in ['TB', 'LR'], \
            "Invalid orientation for vocabview - %s  " % orientation

        picker = make_picker(self.factory)

        G = pgv.AGraph(directed=True, strict=False, dpi=dpi, **kw)
        G.graph_attr['splines'] = True
        G.graph_attr['rankdir'] = orientation

        G.graph_attr['label'] = '%s (from %s)' % (self.package, self.root)

        for e, f, p in self.edges:
            # add extra spacing to avoid labels overlapping lines
            edge_label = ' %s ' % p
            G.add_edge(e, f, label=edge_label, #headlabel=edge_head_label,
                       labeldistance=2., labelfloat=False, labelangle=45.)

        if coloured:
            for node in self.nodes:
                gn = G.get_node(node)
                gn.attr['fillcolor'] = picker.colourise(node)
                gn.attr['style'] = 'filled'

        if key:
            #G.add_subgraph(name='key', style='filled', color='black')
            # = G.subgraph_root('key')
            #for k in allowed_base_classes:
            #    X.add_node(k, fillcolor=picker.colourise(k), style='filled')
                #gn = X[0].get_node(k)
                #gn.attr['fillcolor'] = picker.colourise(k)
                #gn.atr['style'] = 'filled'
            abc = ['classes', 'enums']
            nodes = [str(k) for k in abc]
            print nodes
            G.add_subgraph(nbunch=nodes, name='key', colour='black', style='lightgrey', label='abc')

        G.layout(layout)

        file_name = '%s%s.%s' % (file_base, self.root, fmt)

        G.draw(file_name, fmt)

if __name__=="__main__":
    v = VocabView('cmip6', 'Ocean', 3)
    v.plot('test_lr_', orientation='LR')
    v = VocabView('cmip6', 'Ocean')
    v.plot('test_td_')

