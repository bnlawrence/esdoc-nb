import unittest
from esdoc_nb.mp.core.lib.factory import cimFactory


class Test_Constraints(unittest.TestCase):

    """ Test setting and using constraints on real instances of vanilla CIM2"""

    def setUp(self):
        self.factory = cimFactory()
        self.factory.add_extension_package('cmip6')

        self.test_classes = {
            'pythonic_constant': ('OceanGridHorizontalGrid', 'description'),
            #'cim2_constant': None,
            'cim2_list': ('Ocean','simulates'),
            'cardinality_zero': ('OutputRequirement', 'additional_requirements'),
            #'cardinality_other': 'SimulationPlan',
            #'mutually_exclusive': 'InitialisationRequirement'
        }


    def _basic_checks(self, test_origin):
        """ Tests presence of test class and constraint therein. Failures here
        are failures in the choice of test class or the test class definitions """
        klass, argument = self.test_classes[test_origin]
        assert klass in self.factory.classes,'test class %s not in factory ' % klass
        assert 'constraints' in self.factory.fullset[klass], 'no constraints in '+ klass
        return klass, argument

    def _find_constant_constraint(self, klass, argument):
        """ Finds a particular constant constraint in the factory constructors"""
        constructor = self.factory.fullset[klass]
        constraints = constructor['constraints']
        result = None
        for c in constraints:
            if c[0] == 'constant' and c[1] == argument:
                result = c[2]
                break
        return result

    def test_constant_value(self):
        """ test setting a pythonic constant value"""
        klass, argument = self._basic_checks('pythonic_constant')
        instance = self.factory.build(klass)
        value = getattr(instance, argument)
        comparison = self._find_constant_constraint(klass, argument)
        self.assertEqual(value,comparison,'Instance value not set to the constraint')

    def test_cim2_list(self):
        """ Test setting a set of CIM2 or extension classes as a constant value"""
        klass, argument = self._basic_checks('cim2_list')
        instance = self.factory.build(klass)
        value = getattr(instance, argument)
        values = [v.cimType for v in value]
        comparison = self._find_constant_constraint(klass, argument)
        self.assertEqual(values, comparison, 'Instance list not of the right types (%s cf %s) ' % (values, comparison))

    def test_cardinality_zero(self):
        """ Test we can constrain cardinality to zero"""
        klass, argument = self._basic_checks('cardinality_zero')
        instance = self.factory.build(klass)
        value = instance.attributes[argument]
        candidate = [self.factory.build(value.target)]
        # this is checking that setattr(instance, argument, candidate) raises an AttributeError
        self.assertRaises(AttributeError, setattr, instance, argument, candidate)

if __name__ == "__main__":
    unittest.main()