from inspect import getmembers, isfunction
import unittest
from copy import copy

from collections import OrderedDict

from definitions import classmap, extension_packages2

cim_class = {
    'extra_conservation_properties': 'conservation_properties',
    'tuning_applied': 'tuning',
}


def camelize(string):
    x = string.split('_')
    if len(x) > 1:
        y = [s.capitalize() for s in x]
    else:
        y = [x[0][0:1].capitalize(),x[0][1:]]
    return ''.join(y)

class PropertyType:
    """ Defines the properties of a "target" type string as it appears in the schema definitions in a property
    definition (e.g. ('property_name','type_string','cardinality','definition').
    """
    def __init__(self, type_string):
        """
        :param type_string: A schema definition type string
        :return: An object with the following attributes:
           link: boolean: target will be represented by a "doc_reference" element rather than the actual element.
           package: string: the package in which the target type exists
           property_type: the actual target property type.
           camel: target property type converted to camel case
           link_constraint_type: a constraint type on a remote link, if provided.
        """
        self.link = False
        self.link_constraint_type = None
        self.pythonic = False
        self.definition = type_string

        assert type_string[0] != ' ', 'Property definitions must not begin with a blank: [%s] ' % type_string

        if type_string in classmap:
            self.pythonic = True
            self.package = None
            self.camel = type_string
            self.property_type = type_string
            self.target = type_string
        else:
#            if type_string.startswith('ENUM:'):
#                self.link = True
#                content = type_string.lstrip('ENUM:')
            if type_string.startswith('linked_to'):
                self.link = True
                content = type_string[10:-1]
                if content == '':
                    # any possible document type
                    self.package = 'Any'
                    self.camel = 'Any'
                    self.target = 'Any'
                    return
                else:
                    content = content.split(',')
                if len(content) > 1:
                    if content[1][0] == ' ':
                        print 'Warning: "%s" [%s] needed stripping!' % (content[1], type_string)
                        content[1] = content[1].strip(' ')
                    self.link_constraint_type = PropertyType(content[1])
                content = content[0]
            else:
                content = type_string
            twoparts = content.split('.')
            assert len(twoparts) == 2, 'Invalid property definition - expected package.property_type, got [%s] ' % content
            self.package, self.property_type = twoparts
            self.camel = self._camelise(self.property_type)
            self.target = '%s.%s' % (self.package, self.camel)

    def _camelise(self, dname):
        """ Capitalise ourself """
        if dname in classmap:
            return dname
        return camelize(dname)

    def __eq__(self, other):
        if not isinstance(other,PropertyType):
            return False
        properties = ['link', 'property_type', 'camel', 'package', 'link_constraint_type']
        for p in properties:
            if getattr(self, p) != getattr(other, p): return False
        return True

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return self.camel

def checkenums(constructor):
    """ Check a an enum has appropriate properties"""
    assert 'cimType' in constructor, 'No cimTypeype in %s' % constructor
    estring = ' failure in %s ' % constructor['cimType']
    assert 'type' in constructor, 'Type ' + estring
    assert constructor['type'] == 'enum', 'Not an enum in '+estring
    assert 'members' in constructor, 'No members in '+estring
    assert isinstance(constructor['members'], list), 'Improper member list in '+estring
    for x in constructor['members']:
        try:
            assert len(x) == 2
            assert isinstance(x[0],str)
            assert isinstance(x[1],str)
        except:
            raise AssertionError('Invalid member ::%s:: in '% str(x) + estring)
    return 1

def check_constraints(constructor, cimset):
    """ Implements tests against the constraint specification """

    constraints = constructor['constraints']
    assert isinstance(constraints, list), "Constraints not a list"
    location = 'in %s' % constructor['cimType']
    for c in constraints:

        # first check it's a legal constraint
        assert c[0] in ['regex',
                        'enum-extensibility',
                        'cardinality',
                        'constant',
                        'mutually-exclusive'
                        ], 'Unknown constraint [%s] %s ' % (c[0], location)

        # now check there is a sensible target property (or properties)
        targets = {}
        if cimset:
            available = gather_base_properties(constructor, cimset)
        else:
            available = constructor['properties']
        for a in available:
            targets[a[0]] = a[1]

        if isinstance(c[1],tuple):
            checkable=list(c[1])
        else:
            checkable = [c[1],]

        for t in checkable:
            assert t in targets,\
                "Constraint on non-existent property '%s' from %s %s " % (t, targets.keys(), location)


        # Now check that the constraint constant values exist ...   if not native types.
        # Only raise warnings for these, for now.
        if c[0] == 'constant':
            if isinstance(c[2], list):
                targets = c[2]
            else:
                targets = [c[2],]
            if cimset:
                for t in targets:
                    # the list at the end is for pythonic text and string assignments ...
                    if t not in cimset and c[1] not in ['short_name', 'description', 'realm']:
                        print 'WARNING: cannot find target "%s" for constant constraint [%s] %s ' % (
                            t, c[1], location)

        # Now check each specific sort of constraint

        if c[0] == 'regex':
            pass
            # TODO, how to assert if this is regex
        elif c[0] == 'enum-extensibility':
            assert c[2] in [True, False], \
                "Invalid enum-extensiblity %s %s " % (c[2], location)
        elif c[0] == 'cardinality':
            # TODO, pull out card_check from check_metamodel and use it there and here
            pass
        elif c[0] == 'constant':
            # we don't need to check type, that'll come with assignment
            # but we should check form
            estring = 'Invalid constraint %s %s ' % (c, location)
            assert len(c) in [3,5], estring
            if len(c) == 5:
                assert c[3] == 'extensibility', estring
                assert c[4] in [True, False], estring
        elif c[0] == 'mutually-exclusive':
            assert isinstance (c[1], tuple)
            # TODO check cardinality here too

    return 1


def checkmetamodel(constructor, cimset=None):
    """
    Checks consistency of a class constructor with our metamodel
    :param constructor: A cim class definition constructor
    :param cimset: The complete set of possible cim targets for property target testing
    :return: True if OK
    """

    def card_check():
        """ Check valid cardinality"""
        cardinality = p[2]
        estring = 'for %s in %s ' % (p[0], constructor['cimType'])
        assert isinstance(cardinality,str), 'Wrong type cardinality '+estring
        assert '.' in cardinality, 'No full stop in cardinality '+estring
        bits = cardinality.split('.')
        assert len(bits) == 2, 'Wrong length cardinality' + estring

    try:
        # Next two attributes are not in the basic upstream definition functions but are inserted
        # by the esdoc-nb/mp framework to support the downstream API.
        assert constructor['type'] == 'class', 'Cannot check enums with this function'
        assert 'cimType' in constructor, 'Missing cimType'
        estring = 'in %s' % constructor['cimType']
        assert 'doc' in constructor, 'Missing doc'+estring
        # The rest of these tests are the basic upstream metamodel understood by esdoc-mp
        assert 'is_abstract' in constructor,'Missing is_abstract'+estring
        assert 'properties' in constructor or 'constraints' in constructor, 'Missing properties'+estring
        assert 'base' in constructor, 'Missing base'+estring
        if 'properties' in constructor:
            assert isinstance(constructor['properties'], list), 'Properties not a list'+estring
            for p in constructor['properties']:
                if cimset:
                    assert str(p[1]) in cimset or str(p[1]) in classmap.keys(), \
                        'property target failure for %s %s' % (p[1], estring)
                card_check()

        if 'constraints' in constructor:
            ok = check_constraints(constructor, cimset)

        if 'alternatives' in constructor:
            # Can't rely on this if constructor supplied via factory ...
            assert isinstance(constructor['alternatives'],list), 'Alternatives not a list'+estring
            if cimset:
                for p in constructor['alternatives']:
                    assert str(p) in cimset, 'Non existent alternative class [%s] %s' % (p, estring)

    except AssertionError,e:
        print constructor
        raise
    except Exception, e:
        print constructor
        import traceback
        traceback.print_exc()
        raise Exception(e)
    return 1

def gather_base_properties(constructor, cimset):
    """ Gather all properties (included inherited properties) for a class
    :param klass: A cim class constructor
    :param cimset: A complete set of cim class constructors
    :return: List of all properties for the given klass
    """
    assert 'base' in constructor,'No base class in constructor %s' % constructor
    r = []
    if 'properties' in constructor:
        r = copy(constructor['properties'])
    bases = gather_base_heirarchy(constructor, cimset)
    for b in bases:
        if 'properties' in cimset[b]:
            r += copy(cimset[b]['properties'])
    return r

def gather_base_heirarchy(constructor, cimset, follow=True):
    """For a given constructor, find the base hierarchy from within a complete set of
     constructors, cimset."""
    if 'members' in constructor:
        return []   # it's an enum
    assert 'base' in constructor, 'No base class in constructor %s' % constructor
    stack = []
    if constructor['base']:
        base = str(constructor['base'])
        stack.append(base)
        if follow:
            assert base in cimset, 'Unknown base class [%s] found in [%s]' % (
                base, constructor['cimType'])
            stack += gather_base_heirarchy(cimset[base], cimset, follow)
    return stack

def function2dict(f):
    """ Used to go from the cim defined as functions to the cim defined as a dictionary
    :param f: a constructor function
    :return: (key,c) a cimtype and it's constructor dictionary
    """
    key = camelize(f[0])
    try:
        c = f[1]()
        if f[1].__doc__ != '':
            c['doc'] = f[1].__doc__
        else:
            # If no real doc string,
            # assume the class name is useful in it's own right
            c['doc'] = key.replace('_',' ')
        c['cimType'] = key
        # The metamodel expects these, but they get in the way of
        # vocabulary construction (since they have no value in
        # normal vocabularies.
        if 'vocab_status' in c:
            if 'properties' not in c:
                c['properties'] = []
            if 'is_abstract' not in c:
                c['is_abstract'] = False
    except Exception, e:
        raise Exception, str(e)+'( for %s)' % key
    return key, c


def fix_properties(constructor):
    """Take a raw constructor and replace all the property names with PropertyTypes """
    constructor = copy(constructor)
    try:
        if 'base' in constructor:
            if constructor['base']:
                constructor['base'] = PropertyType(constructor['base'])
        if 'alternatives' in constructor:
            constructor['alternatives'] = [str(PropertyType(a)) for a in constructor['alternatives']]
        if 'properties' in constructor:
            fixed = []
            for p in constructor['properties']:
                buf = list(p)
                if buf[1].startswith('ENUM:'):
                    # Enumeration
                    buf[1] = buf[1].lstrip('ENUM:')
                else:
                    buf[1] = PropertyType(buf[1])
                fixed.append(tuple(buf))
            constructor['properties'] = fixed
        if 'constraints' in constructor:
            for c in constructor['constraints']:
                if c[1] == 'include':
                    c[2] = [str(PropertyType[c2item]) for c2item in c[2]]
    except:
        print 'Failed to fix_properties for constructor'
        print constructor
        raise
    return constructor


def get_from_modules(pdict):
    """ Load constructors from a dictionary of modules """
    c = {}
    pkg = {}
    for p in pdict:
        module = pdict[p]
        function_list = [o for o in getmembers(module) if isfunction(o[1])]
        pkg[p] = []
        for f in function_list:
            key, constructor = function2dict(f)
            c[key] = fix_properties(constructor)
            pkg[p].append(key)
    return c, pkg

def decode_extensions_syntax2(package_name):
    """Decode an extension package in the compact notation and turn into
    standard form constructors. Note that these constructors have one
    significant problem which needs to be fixed later: properties which
    are single valued, but non-pythonic (i.e. are CIM2 or CIM2 extension
    instances) will appear as a list, even though they are single-valued.
    We can't fix this here, because we don't want to import the CIM here.
    """
    debug = 0

    def _finalise(n, c):
        """ Load constructor and if necessary, log it """
        pt = PropertyType('%s.%s'%(package_name,n))
        assert n == c['cimType'], "Key value failure for %s (%s) " % (n, c)
        c['cimType'] = pt.camel
        constructors[pt.camel] = c
        names.append(pt.camel)
        if not debug:
            return
        print '\nDone {}: {}'.format(n, c)
        if 'base' in c:
            print '({} base: {})'.format(pt.camel, c['base'])
        else:
            print '(enum)'

    def _short_name(cimType):
        # 'ocean_key_properties' -> 'Ocean key properties'
        cimType = cimType.capitalize()
        return cimType.replace('_', ' ')


    def _fix_constraint(prefix, name, value, details=''):
         """ handling three cases,
         value could be a native python instance, in which case return as is,
         or it could single value, but we'll only know that if we have the CIM to tell us
         since the syntax always gives us a list here, or it could be a real list
         of stuff all of which are specialisations """
         # TODO, meanwhile, assume always ok to be a list ... need the CIM here to do better.
         if isinstance(value, list):
            if name == 'details':
                r = [PropertyType('%s.%s%s_%s' % (package_name, details, prefix, x)).camel for x in value]
            else:
                r = [PropertyType('%s.%s_%s' % (package_name, prefix, x)).camel for x in value]
            return r
         else:
            # not a specialisation
            return value


    assert package_name in extension_packages2, 'Unknown extension package %s ' % package_name
    constructors = {}
    names = []

    if debug:
        print 'Expecting to handle:'
        for k,s in extension_packages2[package_name].iteritems():
            print k
        print ' '

    for klass, stuff in extension_packages2[package_name].iteritems():
        
        if debug:
            print '--------------------\nklass: %s \n %s ' % (klass, stuff)

        master_cimType = klass.split('.')[-1]
        
        top_level_stuff = []
        lower_level_stuff = []

        enumerations = []

        for variable, value in stuff:

            # Ignore file metadata variables and internal variables
            if variable in ('AUTHORS', 'CONTACT', 'QC_STATUS', 'ID',
                            '_TYPE', 'OrderedDict' ):
                continue

            if variable == 'DETAILS':
                assert(isinstance(value, OrderedDict),
                       klass+' file variable '+variable+' must be an OrderedDict'+'. Got: '+value.__class__.__name__)
                lower_level_stuff.append(('DETAILS', value))
                top_level_stuff.append(('DETAILS', value.keys()))

            elif variable.endswith('_DETAILS'):
                assert(isinstance(value, OrderedDict),
                       klass+' file variable '+variable+' must be an OrderedDict'+'. Got: '+value.__class__.__name__)
                lower_level_stuff.append((variable, value))

            elif variable in ('ENUMERATIONS', 'SUB_PROCESSES',
                              'EXTENT', 'DISCRETISATION', 'RESOLUTION',
                              'EXTRA_CONSERVATION_PROPERTIES', 'TUNING_APPLIED'):
                assert(isinstance(value, OrderedDict), 
                       klass+' file variable '+variable+' must be an OrderedDict'+'. Got: '+value.__class__.__name__)
                lower_level_stuff.append((variable, value))

            else:
                assert(isinstance(value, (list, basestring, int, long, float, bool)),
                       'File variable '+variable+' must be one of list, str, int, float, bool')
                top_level_stuff.append((variable, value))

        # ------------------------------------------------------------
        # Deal with SCIENTIFIC_REALM, GRID, KEY_PROPERTIES, PROCESS
        # ------------------------------------------------------------
#        print 'TPLS=', top_level_stuff, '\n\n\n'
#        print 'lPLS=', lower_level_stuff, '\n\n\n'

        base = 'science.'
        if master_cimType.endswith('key_properties'):
            # science.key_properties
            base += 'key_properties'
        elif master_cimType.endswith('grid'):
            # science.grid
            base += 'grid'
        elif '_' in master_cimType:
            # science.process
            base += 'process'
        else:
            # science.scientific_realm
            base += 'scientific_realm'

        compact = {
            'type': 'class',
            'base': base,
            'cimType': master_cimType,
            'is_abstract': False,
            'doc': '[{}] constrained version of [{}]'.format(package_name, base),
            'constraints': [('constant', name.lower(), _fix_constraint('', name.lower(), value, details=master_cimType))
                            for name, value in top_level_stuff],

        }

        _finalise(master_cimType, compact)

        for variable, value in lower_level_stuff:

            if variable == 'ENUMERATIONS':
                # ----------------------------------------------------
                # Deal with ENUMERATIONS
                #
                # Do this first so that 
                # ----------------------------------------------------

                for cimType, enumeration in value.iteritems():        
                    assert isinstance(enumeration, dict), "Enumeration must be a dictionary"

                    assert('description' in enumeration,
                           "Enumeration must have 'description'. Got: "+str(enumeration))

                    assert('members' in enumeration,
                           "Enumeration must have 'members'. Got: "+str(enumeration))

                    assert(len(enumeration) == 2,
                           "Enumeration must have 'description' and 'members' and nothing else. Got: "+str(enumeration))

                    cimType = master_cimType+'_'+cimType
                    #enumeration['short_name'] = _short_name(cimType)

                    compact = {
                        'type':    'enum',
                        'cimType': cimType,
                        'doc':     enumeration['description'],
                        'members': enumeration['members']
                    }

                    _finalise(cimType, compact)
                    enumerations.append(cimType)

            elif variable.endswith('DETAILS'):
                # ----------------------------------------------------
                # Deal with DETAILS, SUB_PROCESSES_DETAILS,
                # RESOLUTION_DETAILS, etc.
                # ----------------------------------------------------
                for cimType, details in value.iteritems():
                    # Create a science.detail
                  
                    if isinstance(details, tuple) and len(details) == 3:
                        # E.g. DETAILS['name'] = ('value', 'str', 'description')
                        description = details[2]
                        properties  = [(cimType,) + details]
                    else:
                        # E.g. DETAILS['some_details'] = {'properties': [...]}
                        assert (isinstance(details, dict),
                                'Details must be 3-tuple or dictionary. Got: '+str(details))

                        assert('properties' in details and  len(details) <= 2,
                               "Details dictionary must have 'properties', optional 'description' and nothing else. Got: "+str(details))

                        if len(details) == 2:
                            assert('description' in details,
                                   "Details dictionary must have 'properties', optional 'description' and nothing else. Got: "+str(details))

                    
                        description = details.get('description', _short_name(master_cimType+'_'+cimType))
                        properties  = details['properties']

                    properties2 = []
                    for i, (p, t, c, d) in enumerate(properties):
                        if t.startswith('ENUM:'):
                            t = t.replace('ENUM:', 'cmip6.'+master_cimType+'_', 1)
                        properties2.append((p, t, c, d))
                    properties = properties2                
               
                    base = 'science.detail'
                    cimType = master_cimType + '_' + cimType
                 
                    compact = {
                        'type': 'class',
                        'cimType': cimType,
                        'is_abstract': False,
                        'base': base,
                        'doc': '[{}] constrained version of [{}]'.format(package_name,base),
                        'properties': properties,
                        'constraints': [
                            ('constant', 'short_name', _short_name(cimType)),
                            ('constant', 'description', description),
                        ],
                    }
                    _finalise(cimType, compact)

            else:
                # ----------------------------------------------------
                # Deal with model SUB_PROCESSES, RESOLUTION,
                # DISCRETISATION, EXTENT, etc.
                #
                # E.g. ('RESOLUTION', OrderedDict([('resolution',
                #       {'description': '',
                #        'details': ['thickness_of_surface_level']})])
                # ----------------------------------------------------
                for cimType, item in value.iteritems():
                    assert isinstance(item, dict), variable.capitalize()+" must be a dictionary"

                    assert('description' in item and len(item) <= 2,
                           variable.capitalize()+" dictionary must have 'description', optional 'details' and nothing else. Got: "+str(item))
                    assert('details' in item or len(item) == 1,
                           variable.capitalize()+" dictionary must have 'description', optional 'details' and nothing else. Got: "+str(item))

                    cimType = master_cimType+'_'+cimType
                    item['short_name'] = _short_name(cimType)
                    base = 'science.'+cim_class.get(variable.lower(), variable.lower())

                    compact = {
                        'type': 'class',
                        'cimType': cimType,
                        'is_abstract': False,
                        'base': base,
                        'doc': '[{}] constrained version of [{}]'.format(package_name,base),
                        'constraints': [('constant', name, _fix_constraint(master_cimType, name, value))
                                        for name, value in item.iteritems()],
                    }

                    _finalise(cimType, compact)

    # now fix all those property types
    for n in constructors:
        constructors[n] = fix_properties(constructors[n])

    return names, constructors

def decode_extensions(package_name):
    """ Decode an extension package in the compact notation and turn into standard form
    constructors"""
    assert package_name in extension_packages,'Unknown extension package %s ' % package_name
    constructors = {}
    names = []
    for klass in extension_packages[package_name]:
        print '--------------------\nklass=',klass
        compact = copy(klass[1])
        name = camelize(klass[0])
        # unpack compact definitions
        if 'members' in compact:
            compact['type'] = 'enum'
            compact['base'] = None
            compact['doc'] = compact['name']
            compact['cimType'] = name
        else:
            compact['type'] = 'class'
            compact['cimType'] = name
            compact['is_abstract'] = False
            compact['base'] = compact['base']
            compact['doc'] = '(cmip6 constrained version of %s)' % compact['base']
            # how handle implied constraints
            if 'values' in compact:
                compact['constraints'] = []
                for key in compact['values']:
                    print key
                    compact['constraints'].append((key, 'value', compact['values'][key]))

        print 'compact =',compact
        print
        constructors[name] = fix_properties(compact)
        print 'constructors[name] =',constructors[name]
        print
        names.append(name)
    return names, constructors


class TestPackageSplit(unittest.TestCase):
    """ test all the various possibilites for a type_string ... """

    bad_strings = ['numerical_experiment',
                   'linked_to(scientific_domain)',
                   'linked_to(designing.numerical_experiment,constraint_vocab)']

    # good strings:
    normal = 'designing.numerical_experiment'
    linked = 'linked_to(science.scientific_domain)'
    complicated = 'linked_to(designing.numerical_experiment,designing.constraint_vocab)'

    def test_bad_strings(self):
        for string in self.bad_strings:
            self.assertRaises(AssertionError, PropertyType, string)

    def test_normal(self):
        x = PropertyType(self.normal)
        self.assertEqual(x.property_type, 'numerical_experiment')
        self.assertEqual(x.camel, 'NumericalExperiment')
        self.assertEqual(x.package, 'designing')
        self.assertFalse(x.link)

    def test_linked(self):
        x = PropertyType(self.linked)
        self.assertEqual(x.property_type,'scientific_domain')
        self.assertTrue(x.link)

    def test_complicated(self):
        x = PropertyType(self.complicated)
        self.assertTrue(x.link)
        y = PropertyType('designing.constraint_vocab')
        self.assertEqual(x.link_constraint_type, y)


if __name__=="__main__":
    unittest.main()
