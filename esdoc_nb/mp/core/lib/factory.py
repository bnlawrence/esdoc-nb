from definitions import classmap, class_packages, enum_packages
from meta import get_from_modules, gather_base_heirarchy, gather_base_properties, \
    decode_extensions_syntax2
from uml import umlClass, umlEnum, umlProperty
from datetime import datetime
import uuid


class cimFactory(object):

    """ This class defines the CIM eco system and the internal classes, and
    allows one to build class instances."""

    # Try and insulate the notebook world from the esdoc-mp world by ensuring
    # that in esdoc land, everything that was abc_def is now AbcDef.

    def __init__(self, default=True):

        """ CIM eco system constructor. Default is to construct the
        basic CIM. If not default, construct as an empty factory."""

        self.classes = {}
        self.enums = {}
        self.packages = {}
        self.alternatives = {}

        if default:
            self.include(class_packages)
            self.include(enum_packages)

        self._build_classtree()


    def __contains__(self, klass):
        """ Allows one to determine if klass is in the CIM system """
        if klass not in self.classes and klass not in self.enums:
            return False
        return True

    def _load_constructors(self, constructors):
        """ Load constructors into internal class and enum dictionaries """
        # At the moment classes and enums are mixed
        for k in constructors:
            if constructors[k]['type'] == 'class':
                self.classes[k] = constructors[k]
            elif constructors[k]['type'] == 'enum':
                self.enums[k] = constructors[k]

    def _build_classtree(self):
        """ If we want to find all subclasses of any given class, it's better
        we have an index built when we instantiate the class."""
        self.alternatives = {}
        self.bases = {}
        for k in self.classes:
            if self.classes[k]['base']:
                base = str(self.classes[k]['base'])
                if base in self.bases:
                    self.bases[base].append(k)
                else:
                    self.bases[base] = [k,]
            else:
                self.bases[base] = []

    def build(self, klass, initialise_metadata=False, author=None):
        """ Makes and returns an instance of klass, if
         initialise_author and klass is a document, add a
         metadata record using author if provided."""
        if klass in self.classes:
            v = umlClass(self.classes[klass], self.fullset)
            if initialise_metadata and v.isDocument:
                meta = meta_setup(self.classes, author=author)
                v.meta = meta
            return constrain_instance(v, self)
        elif klass in self.enums:
            v = umlEnum(self.enums[klass])
            return v
        else:
            raise ValueError('Cannot find constructor for %s in factory' % klass)

    def shim(self, klass):
        """ Do we want an instance of a klass, or a link to a potential klass instance?
        The answer depends on whether it would be a document klass instance or not. If
        it would be, then we want a CimLink aka DocReference."""
        if self.isdoc(klass):
            link = self.build('DocReference')
            link.type = klass
            nil_template = self.build('NilReason')
            nil_template.set('nil:template')
            link.name = nil_template
            link.linkage = nil_template
            return link
        else:
            return self.build(klass)

    def find_base_classes(self, klass, follow=True):
        """For a given cim type, find base classes"""
        if klass in self.classes:
            d = self.classes[klass]
        elif klass in self.enums:
            print 'No base classes for enums (looking for %s)'%klass
            return []
        else:
            raise ValueError('Base class requested for unknown klass : %s' % klass)
        return gather_base_heirarchy(d, self.fullset)

    def find_associates(self, klass, documents_only=False, get_base=True, get_properties=True):
        """ For a given CIM class, klass, find all other classes which
        are associated *from* that class.
        If get_base, then include all base classes,
        If get_properties, then include all property classes.
        if documents_only, only return document associates.
        """
        extras = []
        if klass in self.enums: return extras
        assert klass in self.classes, 'What to do with [%s]?' % klass
        model = self.classes[klass]
        if get_base and model['base']:
            extras += self.find_base_classes(model['cimType'])
        if get_properties and 'properties' in model:
            for p in model['properties']:
                klass = str(p[1])
                if klass in self and klass not in extras:
                    if not documents_only or (documents_only and self.isdoc(klass)):
                        extras.append(klass)
        return extras

    def isdoc(self, klass):
        """ Convenience method to return whether or not a klass has metadata, ie
        conforms to the CIM2 document stereotype, without needing to build the
        class first."""
        if klass not in self: return False
        if klass in self.enums: return False
        assert 'is_abstract' in self.classes[klass],'No abstract definition in %s' % klass
        if self.classes[klass]['is_abstract']: return False
        result = 'meta' in [p[0] for p in gather_base_properties(self.classes[klass], self.fullset)]
        return result

    def include(self, package_list):
        """  Include content from specfic packages
        :param package_list: A list of domain specific class constructors
        """
        constructors, packages = get_from_modules(package_list)

        self._load_constructors(constructors)

        for p in packages:
            if p in self.packages:
                self.packages[p] += packages[p]
            else:
                self.packages[p] = packages[p]

        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def add_extension_package(self, package_name):
        """Add extension package using compact notation"""
        names, constructors = decode_extensions_syntax2(package_name)
        self.packages[package_name] = names
        self._load_constructors(constructors)
        self.fullset = self.classes.copy()
        self.fullset.update(self.enums)

    def remove_package(self, package_name):
        assert package_name in self.packages, "Cannot remove package we haven't got!"
        for k in self.packages[package_name]:
            del self.classes[k]
            del self.fullset[k]
        del self.packages[package_name]


    def find_equivalent_classes(self, klass):
        """ Find all siblings and children of this class, that is, classes which could be used
        instead of this one. That is, find all subclasses of the base of this one, and any subclasses
        of this one."""
        base = self.classes[klass]['base']
        if base:
            return self.bases[base]+self.bases[klass]
        else:
            return self.bases[klass]

def constrain_instance(obj, factory):
    """Once an obj has been instantiated, any value constraints should be applied.
    (Cardinality and cross property constraints are handled direction in the instance).
    Currently understands constraints of the form
            ('constant', property_name, value or list)
    """

    def fix_decoder_lists(target, value):
        """ the decoder for syntax2 doesn't have the CIM, and so returns all CIM classes and
        specialisations in a list, whether or not the target property is single
        or multi-valued. We fix that here, allowing for the possibility that
        things don't come via the decoder."""
        if target.single_valued and isinstance(value,list):
            assert len(value) == 1, 'Mismatch between specialised constraint (%s) and target (%s)' % (
                value, target
            )
            return value[0]
        else:
            return value

    for c in obj.unset_constraints:
        target_property = c[0]
        target_value = c[1]
        assert target_property in obj.attributes, 'Attempt to constrain property %s not found in %s' % (
                    target_property, obj.cimType)
        target = obj.attributes[target_property]
        if target.single_valued:
            target_value = fix_decoder_lists(target,target_value)
            if target.pythonic:
                value = target_value
            else:
                # special case for cimtext
                if target.target == 'Cimtext':
                    value = factory.build('Cimtext')
                    value.text = target_value
                else:
                    value = factory.shim(target_value)
        else:
            value = []
            for tv in target_value:
                if target.pythonic:
                    value.append(tv)
                else:
                    value.append(factory.shim(tv))
        target.set(value)


    return obj


def make_cim_instance(cimtype, factory=None, initialise_metadata=True):
    """ Make a cim instance,
    either using the default factory class (factory = None),
    or using whatever factory is currently in use (with the provided factory). """
    if factory is None:
        factory = cimFactory()
    assert cimtype in factory.fullset,'Cannot build %s ' % cimtype
    instance = factory.build(cimtype, initialise_metadata=initialise_metadata)
    return instance


def convert_from_pyesdoc(doc, target_type=None):
    """ Converts a pyesdoc class instance into a pseudo_mp umlClass instance
    :param doc: a document encoded as a pyesdoc class instance
    :param target_type: if known, the target type
    :return doc: a notebook pseudo_mp umlClass instance
    """
    # We normally know the target_type once we're dealing with the attributes
    # of a class_instance, it's only the "top" class instance we would parse.
    if not target_type:
        target_type = doc.type_key.split('.')[-1]
    target = make_cim_instance(target_type)
    # First check if this is an enum string value, if so, just set the value and leave
    if isinstance(doc, str):
        target.set(doc)
        return target
    # OK, now we know it's a class we're dealing with:
    for a in target.attributes:
        umlp = target.attributes[a]
        attr = umlp.name
        value = getattr(doc, attr)
        if value is None:
            setattr(target, attr, value)
        elif umlp.target not in classmap:
            if umlp.single_valued:
                setattr(target, attr, convert_from_pyesdoc(value, umlp.target))
            else:
                setattr(target, attr, [convert_from_pyesdoc(v, umlp.target) for v in value])
        else:
            setattr(target, attr, value)
    return target


def makeCimInstanceFromSet(cimtype, cimset):
    """ Makes a cim instance of cimtype from within a set of possible cimtypes: cimset """
    cimtype = package_split(cimtype)[1]
    assert cimtype in cimset,'%s not in %s' % (cimtype,sorted(cimset.keys()))
    constructor = cimset[cimtype]
    assert 'type' in constructor
    assert constructor['type'] in ['class', 'enum']
    if constructor['type'] == 'class':
        return umlClass(constructor)
    elif constructor['type'] == 'enum':
        return umlEnum(constructor)


def makeQuickText(content):
    ''' Given some plain text, turn it into a cimtext object '''
    c = make_cim_instance('Cimtext')
    tc = make_cim_instance('TextCode')
    tc.set('plaintext')
    c.content_type = tc
    c.content = content
    return c

def imeta_setup(constructor_set, source='nb-uml'):
    """ Set up internal metadata """
    meta = umlClass(constructor_set['DocMetaInfo'], constructor_set)
    meta.id = str(uuid.uuid1())
    meta.source = source
    meta.create_date = datetime.now().isoformat(' ')
    meta.update_date = meta.create_date
    meta.version = 1
    return meta

def meta_setup(constructor_set, author=None):
    """ Setup a metadata instance for a CIM document
    :param constructor_set: Set of possible CIM entities
    :param author: Author of this metadata description
    """

    meta = umlClass(constructor_set['Meta'], constructor_set)
    imeta = imeta_setup(constructor_set)
    if author:
        imeta.author = author
        meta.metadata_author = author

    meta._internal_metadata = imeta

    return meta


