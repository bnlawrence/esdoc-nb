__author__ = 'BNL28'
import sys
from inspect import getmembers, ismodule

def get_items(base='cmip6'):

    """ Parse the schema directory to return a set of sub-modules """

    results = {}
    debug = 0

    base_module = __import__(base)
    if debug: print 'BASE_MODULE =', base_module
    entities = sys.modules[base].__all__
    qualified_entities = ['%s.%s' % (base, e) for e in entities]
    if debug: print 'QUALIFIED_ENTITIES =',qualified_entities
    map(__import__, qualified_entities)
    if debug: print 'BASE =', base
    for s in sys.modules:
        if s.startswith(base+'.'):
            if debug: print 'S =',s
            try:
                m = sys.modules[s]
                contents = getmembers(m)
                results[m.__name__] = [o for o in contents 
                                       if not (o[0].startswith('__') or
                                               o[0] == 'OrdreedDict')]
                if debug: print  '\nRESULTS['+m.__name__+'] = ',results[m.__name__]
            except AttributeError as error:
                if debug: print 'PASSED on ',s, error
                pass

#    print 'RESULTS=', results
    return results

if __name__ == "__main__":
    # you might run this to make sure all your extension modules import!
    r = get_items()
    for i in r:
        print i, r[i]
