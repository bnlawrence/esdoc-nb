# The complete set of CIM2 CMIP6 extension files
print 'hello from the cmip6 init.py'

__all__ = [
    'ocean_grid',
    'ocean_key_properties',
    'ocean',
    'ocean_timestepping_framework',
#    "atmosphere",
#    "atmosphere_cloud_scheme",
#    "atmosphere_cloud_simulator",
#    "atmosphere_dynamical_core",
#    "atmosphere_gravity_waves",
#    "atmosphere_grid",
#    "atmosphere_key_properties",
#    "atmosphere_microphysics_precipitation",
#    "atmosphere_radiation",
#    "atmosphere_turbulence_convection",
]
