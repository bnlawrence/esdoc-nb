AUTHOR_GUIDE = 'URL on wordpress site of useful info for authors "CMIP6 specilaisations author guide". This page will be a generic guide on how to fill in a REALM, PROCESS, SUB_PROCESS, SUB_PROCESS_DETAILS, etc. http://cmip6.specialisation.guide/process.html'

ID = None

CONTACT = None

AUTHORS = None

TYPE = 'cim.2.science.key_properties'

# ====================================================================
# KEY PROPERTIES: PROPERTIES
# ====================================================================
DESCRIPTION = None

# ====================================================================
# KEY PROPERTIES: DETAILS
# ====================================================================
DETAILS = {}

# ====================================================================
# KEY PROPERTIES: EXTENT
# ====================================================================
EXTENT = {}

# ====================================================================
# KEY PROPERTIES: RESOLUTION
# ====================================================================
RESOLUTION = {}

# ====================================================================
# KEY PROPERTIES: TUNING
# ====================================================================
TUNING = {}

# ====================================================================
# KEY PROPERTIES: CONSERVATION
# ====================================================================
CONSERVATION = {}

# ====================================================================
# KEY PROPERTIES: ENUMERATIONS
# ====================================================================
ENUMERATIONS = {}
