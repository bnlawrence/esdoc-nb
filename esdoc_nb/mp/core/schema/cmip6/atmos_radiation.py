__author__ = 'BNL28'
version = '0.0.1'

# 2) Need to make clear the list of prognostic and diagnostic variables
# should correspond to the actual model variables in the output. (Still TODO).

atmos_radiation = {
    'base': 'science.process',
    'values': {
        'name': 'Radiation',
        'context':  'The properties and algorithms of the representation of radiation within the atmosphere',
        'id': 'cmip6.atmos.rad',
        'sub-processes': ['long_wave', 'short_wave',],
        'algorithms': ['GHG_variables', 'sw_cloud_variables'],
        },
    }

long_wave = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Long Wave Radiation',
        'context': 'Key properties of the simulation of long wave radiation in the atmsphere',
        'id': 'cmip6.atmos.rad.lw',
        'details': ['lw_properties', ],
        }
    }

short_wave = {
    'base': 'science.sub_process',
    'values': {
        'name': 'Short Wave Radiation in Atmosphere',
        'context': 'Key properties of the simulation of long wave radiation in the atmsphere',
        'id': 'cmip6.atmos.rad.sw',
        'details': ['sw_properties', ],
        }
    }

GHG_variables = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Greenhouse Gases used in Radiation',
         'id':'cmip6.atmos.rad.alg.ghg',
         'context': 'Describe how (and which) greenhouse gases are used in the '
                        'radiation simulation',
        }
    }

sw_cloud_variables = {
    'base': 'science.algorithm',
    'values':
        {'name': 'Aerosols used in Radiation',
         'id': 'cmip6.atmos.rad.alg.aer',
         'context': 'Describe how (and which) aerosols are used in the '
                        'radiation simulation',
        }
    }

lw_properties = {
    'base': 'science.detail',
    'values':
        {'context': 'Key properties of long wave radiation simulation in atmosphere',
         'id': 'cmip6.atmos.rad.lw.props',
         'name': 'Key properties of Long Wave Radiation Simulation',
         'select': 'scheme',
         'from_vocab': 'cmip6.atmos.rad.lw.props.scheme.%s' % version,
         'with_cardinality': '0.N',
         },
    'properties':
        [('long_wave_radiation_timestep','int','1.1',
            'timestep (s) of long-wave timestep in radiation'),
        ('Morcrette based','bool','1.1',
            'Is LW radiation scheme based on Morcrette method?'),
        ('RRTM based','bool','1.1',
            'Is LW radiation scheme based on RRTM'),
        ('number_of_spectral_intervals','int','0.1',
            'Number of spectral intervals used in long wave radiation'),
        ]
}

# FIXME: Check these categories makes sense and add definitions.
# FIXME: Find a relevant publication. And consider the booleans above.

lw_scheme = {
    'name': 'Categories of long wave radiation scheme',
    'id': 'cmip6.atmos.rad.lw.props.scheme',
    'members':[
        ('Wide-Band Model',None),
        ('K-correlated', None),
        ('Two-Layer Stream', None),
        ('Layer-Interaction', None)
    ]
}

sw_properties = {
    'base': 'science.detail',
    'values':
        {'context': 'Key properties of short wave radiation simulation in atmosphere',
         'id': 'cmip6.atmos.rad.sw.props',
         'name': 'Key properties of short wave scheme',
         'select': 'scheme',
         'from_vocab': 'cmip6.atmos.rad.sw.props.scheme.%s' % version,
         'with_cardinality': '1.1',
         },
    'properties':
        [('short_wave_radiation_timestep', 'int', '1.1',
            'timestep (s) of short-wave timestep in radiation'),
        ('number_of_spectral_intervals', 'int', '0.1',
            'Number of spectral intervals used in short wave radiation'),
        ]
}

sw_scheme = {
    'name': 'Categories of short wave radiation scheme',
    'id': 'cmip6.atmos.rad.sw.props.scheme',
    'members': [
        ('Wide-Band Model', None),
        ('Wide-Band (Fouquart)', None)
    ]
}
