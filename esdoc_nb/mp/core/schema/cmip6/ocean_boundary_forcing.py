__author__ = 'eguil'
version = '0.0.2'

#
# CMIP6 ocean boundary forcing CV
#
# Version history on git/bitbucket#
#
# Top level process
#
ID = 'cmp6.ocean.boundary_forcing'

PROPERITES = {

    'type' : 'science.process',

    'short_name': 'Ocean boundary forcing',
    'description': 'Properties of boundary forcing within the ocean component',
    
    'sub-processes': ['momentum',
                      'tracers'],

    'algorithms': ['surface_pressure',
                   'momentum_flux_correction',
                   'tracers_flux_correction',
                   'wave_effects',
                   'river_runoff_budget',
                   'geothermal_heating'],

    # ----------------------------------------------------------------
    # TOP-LEVEL DETAILS 
    # ----------------------------------------------------------------
  
    # ----------------------------------------------------------------
    # SUB-PROCESSES
    # ----------------------------------------------------------------
    'momentum': {
        'short_name': 'Ocean boundary forcing momentum',
        'description': 'Key properties of momentum boundary forcing in the ocean',
        'details': ['momentum_bottom_friction',
                    'momentum_lateral_friction'],
    },

    'tracers': {
        'short_name': 'Ocean boundary forcing tracers',
        'description': 'Key properties of tracers boundary forcing in the ocean',
        'details': ['tracers_sunlight_penetration',
                    'tracers_surface_salinity_atmos',
                    'tracers_surface_salinity_seaice',],
    },

    # ----------------------------------------------------------------
    # DETAILS OF SUB PROCESSES
    # ----------------------------------------------------------------
    'momentum_bottom_friction': {
        'description': 'Properties of momentum bottom friction in ocean',
        'short_name': 'Properties of momentum bottom friction in ocean',
        'scheme': ('ENUM:mom_bottom_friction_types', '1.1'),        
    },

    'momentum_lateral_friction': {
        'description': 'Properties of momentum lateral friction in ocean',
        'short_name': 'Properties of momentum lateral friction in ocean',
        'scheme': ('ENUM:mom_lateral_friction_types', '1.1'),
    },
    
    'tracers_sunlight_penetration': {
        'description': 'Properties of sunlight penetration scheme in ocean',
        'short_name': 'Properties of sunlight penetration scheme in ocean',
        'scheme': ('ENUM:sunlight_penetration_scheme_types', '1,1'),
        'tracers_sun_ocean_colour': (
            'bool','1.1',
            'Is the ocean sunlight penetration scheme ocean colour dependent ?'),
        'tracers_sun_extinct_depth': (
            'str','0.1',
            'Describe and list extinctions depths for sunlight penetration scheme (if applicable).'),
    },
    
    'tracers_surface_salinity_atmos': {
        'description': 'Properties of surface salinity forcing from atmos in ocean',
        'short_name': 'Properties of surface salinity forcing from atmos in ocean',
        'scheme': ('ENUM:surface_salinity_forcing_types', '1.1'),
    },

    'tracers_surface_salinity_seaice': {
        'description': 'Properties of surface salinity forcing from sea ice in ocean',
        'short_name': 'Properties of surface salinity forcing from sea ice in ocean',
        'scheme': ('ENUM:surface_salinity_forcing_types', '1.1'),
    },

    # ----------------------------------------------------------------
    # ALGORITHMS
    # ----------------------------------------------------------------

    'surface_pressure': {
        'short_name': 'Ocean surface pressure boundary correction',
        'description': 'Describe how surface pressure is transmitted to ocean (via sea-ice, nothing specific,...)',
    },


    'momentum_flux_correction': {
        'short_name': 'Ocean momentum surface flux correction',
        'desription': 'Describe any type of ocean surface momentum flux correction and, if applicable, how it is applied and where.',
    },

    'tracers_flux_correction': {
        'short_name': 'Ocean tracers surface flux correction',        
        'descrption': 'Describe any type of ocean surface tracers flux correction and, if applicable, how it is applied and where.',
    },

    'wave_effects': {
        'short_name': 'Ocean surface wave effects',
        'description': 'Describe if/how wave effects are modelled at ocean surface.',
    },

    'river_runoff_budget': {
        'short_name': 'Ocean river runoff budget',
        'description': 'Describe how river runoff from land surface is routed to ocean and any global adjustment done.',
    },

    'geothermal_heating': {
        'short_name': 'Ocean bottom geothermal heating',
        'desription': 'Describe if/how geothermal heating is present at ocean bottom.',
    },

}

#
#
# CV for enumerated lists
#


ENUMS = {

    'mom_bottom_friction_types': {
        'name': 'Type of momentum bottom friction in ocean',
        'members': [
            ('Linear', 'tbd'),
            ('Non-linear', 'tbd'),
            ('Non-linear (drag function of speed of tides)', 'tbd'),
            ('Constant drag coefficient', 'tbd'),
            ('None', 'tbd'),
            ('Other', 'tbd'),
        ]
    },

    'mom_lateral_friction_types': {
        'name': 'Type of momentum lateral friction in ocean',
        'members': [
            ('None', 'tbd'),
            ('Free-slip', 'tbd'),
            ('No-slip', 'tbd'),
            ('Other', 'tbd'),
        ]
    },

    'sunlight_penetration_scheme_types': {
        'name': 'Type of in ocean',
        'members': [
            ('1 extinction depth', 'tbd'),
            ('2 extinction depth', 'tbd'),
            ('3 extinction depth', 'tbd'),
            ('Other', 'tbd'),
        ]
    },

    'surface_salinity_forcing_types': {
        'name': 'Type of surface salinity forcing in ocean',
        'members': [
            ('Freshwater flux', 'tbd'),
            ('Virtual salt flux', 'tbd'),
            ('Other', 'tbd'),
        ]
    },    
}
