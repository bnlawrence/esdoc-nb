__author__ = 'CLP73'
version = '0.0.1'

#
# This is the candidate standard vocabulary for CMIP6 atmosphere.

# Note that the classes and enums need to be mixed to make it possible to easily develop these
# ab initio in python. If we develop a different toolchain, we can split them.

# Note that vocab_status attribute is used for two reasons: the primary reason is to
# help the test system known what needs to be done in vocabulary development. Classes
# in the CMIP6 package without the attribute, or at the wrong level, are deemed to be
# incomplete. It is also used in the notebook meta system to indicate that classes
# carrying this attribute will not be abstract, and will not carry properties.

#
# Domain definition
#


def _atmos_domain():
    """ Characteristics of the atmosphere simulation"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.scientific_domain',
        'constraints': [
            ('realm', 'value', 'Atmosphere'),
            ('simulates', 'include', ['cmip6.atmos_properties',
                                      'cmip6.atmos_dynamical_core',
                                      'cmip6.atmos_radiation',
                                      'cmip6.atmos_turbulance_convection',
                                      'cmip6.atmos_microphysics_precipitation',
                                      'cmip6.atmos_gravity_waves']),
        ]
    }


#
# Processes follow
#


# Process: Atmosphere: key properties
def _atmos_properties():
    """ This is the catch all generic atmosphere description """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere'),
            ('detailed_properties', 'include', [
                'cmip6.atmos_model_family',
                'cmip6.atmos_basic_approximations',
                'cmip6.atmos_volcanoes_implementation',
                'cmip6.atmos_top_insolation_ozone', ]),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_top_insolation_solar_constant',
                'cmip6.atmos_top_insolation_orbital_parameters',
                'cmip6.atmos_orography', ]),
        ]
    }


# Process: Atmosphere: dynamical core
def _atmos_dynamical_core():
    """ Characteristics of the atmosphere dynamical core processes"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Dynamical Core'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_dynamical_core_timestepping_framework',
                'cmip6.atmos_dynamical_core_discretisation_horizontal',
                'cmip6.atmos_dynamical_core_diffusion_horizontal',
                'cmip6.atmos_dynamical_core_advection_tracers',
                'cmip6.atmos_dynamical_core_advection_momentum',
                'cmip6.atmos_dynamical_core_top_boundary', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_dynamical_core_lateral_boundary',
                'cmip6.atmos_dynamical_core_prognostic_variables', ]),
            ]
     }


# Process: Atmosphere: radiation
def _atmos_radiation():
    """ Characteristics of the atmosphere radiation process"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Radiation'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_radiation_longwave_scheme',
                'cmip6.atmos_radiation_shortwave_scheme', ]),
            ('detailed_properties', 'include', [
                # 'cmip6.atmos_radiation_timestep',
                'cmip6.atmos_radiation_aerosol_types',
                'cmip6.atmos_radiation_ghg_types',
                'cmip6.atmos_radiation_cloud_ice',
                'cmip6.atmos_radiation_cloud_liquid', ]),
            ]
        }


# Process: Atmosphere: turbulence and convection
def _atmos_turbulance_convection():
    """ Characteristics of the atmospheric turbulence and convection"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Convective Turbulence and Clouds'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_boundary_layer_turbulence_scheme',
                'cmip6.atmos_deep_convection_scheme',
                'cmip6.atmos_shallow_convection_scheme',
                'cmip6.atmos_other_convection_scheme', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_deep_convection_scheme_processes',
                'cmip6.atmos_shallow_convection_scheme_processes', ]),
            ]
        }


# Process: Atmosphere: microphysics and precipitation
def _atmos_microphysics_precipitation():
    """Characteristics of cloud microphysics and precipitation"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Cloud Microphysics and Precipitation'),
            ('detailed_properties', 'include', [
                'cmip6.atmos_large_scale_precipitation_scheme',
                'cmip6.atmos_large_scale_precipitation_hydrometeors',
                'cmip6.atmos_cloud_microphysics_scheme',
                'cmip6.atmos_cloud_microphysics_processes', ]),
            ]
        }


# Process: Atmosphere: cloud scheme
def _atmos_cloud_scheme():
    """ Characteristics of the cloud scheme"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Cloud Scheme'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_cloud_scheme_separate_treatment',
                'cmip6.atmos_cloud_scheme_cloud_overlap',
                'cmip6.atmos_cloud_scheme_processes', ]),
            ]
        }


# Process: Atmosphere: cloud simulator
# TODO configure these classes to allow the user to enter strings/integers/lists of strings
def _atmos_cloud_simulator():
    """Characteristics of the cloud simulator"""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Atmosphere Cloud Simulator'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_cloud_simulator_cosp_attributes',
                'cmip6.atmos_cloud_simulator_inputs_radar',
                'cmip6.atmos_cloud_simulator_inputs_lidar',
                'cmip6.atmos_cloud_simulator_isscp_attributes', ]),
            ]
        }


# Process: Atmosphere: gravity waves
def _atmos_gravity_waves():
    """ Characteristics of the parameterised gravity waves in the atmosphere, whether
     from orography or other sources."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.process',
        'constraints': [
            ('name', 'value', 'Parameterised Gravity Waves'),
            ('algorithm_properties', 'include', [
                'cmip6.atmos_orographic_gravity_waves',
                'cmip6.atmos_non_orographic_gravity_waves', ]),
            ('detailed_properties', 'include', [
                'cmip6.atmos_gravity_waves_sponge_layer',
                'cmip6.atmos_gravity_waves_background',
                'cmip6.atmos_gravity_waves_subgrid_scale_orography', ]), ]
        }


#
# Process details follow
#


# Process-detail: Atmosphere: key properties: model family
def _atmos_model_family():
    """Type of atmospheric model."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_model_family_type',
             '1.1', 'type of atmospheric model'), ],
        'constraints': [
            ('heading', 'value', 'Type of atmospheric model'),
            ('vocabulary', 'value', 'cmip6.atmos_model_family_type.%s' % version),
            ('selection', 'from', 'cmip6.atmos_model_family_type', '1.1'), ]
    }


def _atmos_model_family_type():
    """Type of atmospheric model."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('AGCM', 'Atmospheric General Circulation Model'),
            ('ARCM', 'Atmospheric Regional Climate Model'),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: key properties: basic approximations
def _atmos_basic_approximations():
    """Basic approximations made in the atmosphere model."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_basic_approximations_attributes',
             '1.N', 'basic approximations in the atmosphere model'), ],
        'constraints': [
            ('heading', 'value', 'Basic approximations in the atmosphere model'),
            ('vocabulary', 'value', 'cmip6.atmos_basic_approximations_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_basic_approximations_attributes', '1.N'), ]
    }


def _atmos_basic_approximations_attributes():
    """Basic approximations made in the atmosphere."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('primitive equations', None),
            ('non-hydrostatic', None),
            ('anelastic', None),
            ('Boussinesq', None),
            ('hydrostatic', None),
            ('quasi-hydrostatic', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: key properties: volcanoes
def _atmos_volcanoes_implementation():
    """Implementation of volcanoes."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_volcanoes_implementation_method',
             '1.1', 'implementation method for effect of volcanoes'), ],
        'constraints': [
            ('heading', 'value', 'Implementation method for effect of volcanoes'),
            ('vocabulary', 'value', 'cmip6.atmos_volcanoes_implementation_method.%s' % version),
            ('selection', 'from', 'cmip6.atmos_volcanoes_implementation_method', '1.1'), ]
    }


def _atmos_volcanoes_implementation_method():
    """How volcanic effects are modeled in the atmosphere."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('high frequency solar constant anomaly', None),
            ('stratospheric aerosols optical thickness', None),
            ('other', None),
            ('none', None),
        ]
    }


# Process-detail: Atmosphere: key properties: top insolation impact on ozone
def _atmos_top_insolation_ozone():
    """Top of atmosphere insolation impact on ozone."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('boolean', 'cmip6.atmos_top_insolation_ozone', '1.1',
             'impact of top of atmosphere insolation on stratospheric ozone'), ],
        'constraints': [
            ('heading', 'value', 'Impact of top of atmosphere insolation on stratospheric ozone'), ]
    }


# TODO configure this so that the user can answer a boolean
def _atmos_top_insolation_ozone_included():
    """Impact of top of atmosphere radiation on stratospheric ozone."""
    return{
    }


# Process-detail: Atmosphere: key properties: top insolation solar constant
# TODO configure the class to allow the user to enter their own number and text description
def _atmos_top_insolation_solar_constant():
    """Top of atmosphere insolation: solar constant."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_top_insolation_solar_constant_type', '1.1', 'solar constant type'),
            ('real', 'cmip6.atmos_top_insolation_solar_constant_fixed_value', '1.1', 'solar constant fixed value'),
            ('string', 'cmip6.atmos_top_insolation_solar_constant_transient_characteristics', '1.1',
             'solar constant transient characteristics (W m-2)'), ],
        'constraints': [
            [('heading', 'value', 'Solar constant type'),
                ('vocabulary', 'value', 'cmip6.atmos_dynamical_core_top_boundary_condition.%s' % version),
                ('selection', 'from', 'cmip6.atmos_dynamical_core_top_boundary_condition', '1.1'), ],
            [('heading', 'value', 'Fixed value of the solar constant (W m-2)'), ],
            [('heading', 'value', 'Transient characteristics of the solar constant'), ],
             ]
    }


def _atmos_top_insolation_solar_constant_type():
    """Time adaptation of the solar constant."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('fixed', None),
            ('transient', None),
        ]
    }


# TODO allow user to leave this blank if _atmos_top_insolation_solar_constant_type is 'transient'
# TODO configure the class to allow the user to enter a real number
def _atmos_top_insolation_solar_constant_fixed_value():
    """If the solar constant is fixed, enter the value of the solar constant (W m-2)."""
    return{
    }


# TODO allow user to leave this blank if _atmos_top_insolation_solar_constant_type is 'fixed'
# TODO configure the class to allow the user to enter a text description
def _atmos_top_insolation_solar_constant_transient_characteristics():
    """If the solar constant is transient, enter the transient solar constant characteristics."""
    return{
    }


# Process-detail: Atmosphere: key properties: top insolation orbital parameters
# TODO configure the class to allow the user to enter their own number and text description
def _atmos_top_insolation_orbital_parameters():
    """Top of atmosphere insolation: orbital parameters."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_top_insolation_orbital_parameters_type', '1.1', 'orbital parameters type'),
            ('integer', 'cmip6.atmos_top_insolation_orbital_parameters_fixed_reference_date', '1.1',
             'fixed orbital parameters reference date (yyyy)'),
            ('string', 'cmip6.atmos_top_insolation_solar_constant_transient_characteristics', '1.1',
             'transient orbital parameters characteristics'),
            ('selection', 'cmip6.atmos_top_insolation_orbital_parameters_computation_method', '1.1',
             'orbital parameters computation method'), ],
        'constraints': [
            [('heading', 'value', 'Orbital parameters type'),
                ('vocabulary', 'value', 'cmip6.atmos_top_insolation_orbital_parameters_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_top_insolation_orbital_parameters_type', '1.1'), ],
            [('heading', 'value', 'Fixed reference date of the orbital parameters (yyyy)'), ],
            [('heading', 'value', 'Transient characteristics of the orbital parameters'), ],
            [('heading', 'value', 'Orbital parameters computation method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_top_insolation_orbital_parameters_computation_method.%s' % version),
                ('selection', 'value', 'cmip6.atmos_top_insolation_orbital_parameters_computation_method', '1.1'), ],
             ]
    }


def _atmos_top_insolation_orbital_parameters_type():
    """Time adaptation of the orbital parameters."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('fixed', None),
            ('transient', None),
        ]
    }


# TODO allow user to leave this blank if _atmos_top_insolation_orbital_parameters_type is 'transient'
# TODO configure the class to allow the user to enter an integer
def _atmos_top_insolation_orbital_parameters_fixed_reference_date():
    """If the orbital parameters are fixed, enter a reference year for the orbital parameters (yyyy)."""
    return{
    }


# TODO allow user to leave this blank if _atmos_top_insolation_orbital_parameters_type is 'fixed'
# TODO configure the class to allow the user to enter a text description
def _atmos_top_insolation_orbital_parameters_transient_characteristics():
    """If the orbital paramters are transient, describe the transient orbital parameters characteristics."""
    return{
    }


def _atmos_top_insolation_orbital_parameters_computation_method():
    """Method used for computing orbital parameters."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Berger 1978', None),
            ('Laskar 2004', None),
            ('Other', None),
        ]
    }


# Process-detail: Atmosphere: key properties: orography
def _atmos_orography():
    """Orography Characteristics."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_orography_type', '1.1', 'orography type'),
            ('selection', 'cmip6.atmos_orography_changes', '1.N', 'orography changes'), ],
        'constraints': [
            [('heading', 'value', 'Orography type'),
                ('vocabulary', 'value', 'cmip6.atmos_orography_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orography_type', '1.1'), ],
            [('heading', 'value', 'Orography changes'),
                ('vocabulary', 'value', 'cmip6.atmos_orography_changes.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orography_changes', '1.N'), ],
             ]
    }


def _atmos_orography_type():
    """Time adaptation of the orography."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('present-day', None),
            ('modified', None),
        ]
    }


# TODO allow user to leave this blank if the _atmos_orography_type is 'present day'
def _atmos_orography_changes():
    """If the orography type is modified describe the time adaptation changes."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('related to ice sheets', None),
            ('related to tectonics', None),
            ('modified mean', None),
            ('modified variance if taken into account in model (cf gravity waves)', None),
        ]
    }


# TODO format this so that the user can enter an integer
# Process-detail: Atmosphere: dynamical core: timestepping framework
def _atmos_dynamical_core_timestepping_framework():
    """Timestepping framework."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_timestepping_type', '1.1', 'timestepping framework type'),
            ('integer', 'cmip6.atmos_dynamical_core_time_step', '1.1', 'time step (seconds)'), ],
        'constraints': [
            [('heading', 'value', 'Timestepping scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_dynamical_core_timestepping_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_dynamical_core_timestepping_type', '1.1'), ],
            [('heading', 'value', 'Time step (seconds)'), ],
             ]
    }


def _atmos_dynamical_core_timestepping_type():
    """Type of time stepping scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Adam Bashford', None),
            ('explicit', None),
            ('implicit', None),
            ('semi-implicit', None),
            ('leap frog', None),
            ('multi-step', None),
            ('Runge Kutta fifth order', None),
            ('Runge Kutta second order', None),
            ('Runge Kutta third order', None),
            ('other', None),
        ]
    }


# TODO format this so that the user can enter an integer
def _atmos_dynamical_core_time_step():
    """Time step of the model (seconds)."""
    return{
    }


# Process-detail: Atmosphere: dynamical core: horizontal discretisation
def _atmos_dynamical_core_discretisation_horizontal():
    """Horizontal discretisation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_discretisation_horizontal_type', '1.1',
             'horizontal discretisation type'),
            ('selection', 'cmip6.atmos_dynamical_core_discretisation_horizontal_method', '1.1',
             'horizontal discretisation method'),
            ('selection', 'cmip6.atmos_dynamical_core_discretisation_horizontal_order', '1.1',
             'horizontal discretisation function order'),
            ('selection', 'cmip6.atmos_dynamical_core_discretisation_horizontal_pole', '1.1',
             'horizontal discretisation pole singularity treatment'), ],
        'constraints': [
            [('heading', 'value', 'Horizontal discretisation scheme type'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_type.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_type', '1.1'), ],
            [('heading', 'value', 'Horizontal discretisation scheme method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_method.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_method', '1.1'), ],
            [('heading', 'value', 'Horizontal discretisation scheme order'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_order.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_order', '1.1'), ],
            [('heading', 'value', 'Horizontal discretisation pole singularity treatment'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_pole.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_discretisation_horizontal_pole', '1.1'), ],
             ]
    }


def _atmos_dynamical_core_discretisation_horizontal_type():
    """Type of horizontal discretisation scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('spectral', None),
            ('fixed grid', None),
            ('other', None),
        ]
    }


# TODO allow user to leave this blank if _atmos_dynamical_core_discretisation_horizontal_type is 'spectral' or 'other'
def _atmos_dynamical_core_discretisation_horizontal_method():
    """If the scheme type is fixed grid,
    describe the scheme method for the horizontal discretisation scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('finite elements', None),
            ('finite volumes', None),
            ('finite difference', None),
            ('centered finte difference', None),
        ]
    }

# TODO allow user to leave blank if _atmos_dynamical_core_discretisation_horizontal_method is not 'finite difference'
# TODO format this so that the user can enter an integer
def _atmos_dynamical_core_discretisation_horizontal_order():
    """If the scheme method is finite difference or centered finite difference
    describe the scheme order of the finite difference method used by the horizontal discretisation scheme."""
    return{
    }


def _atmos_dynamical_core_discretisation_horizontal_pole():
    """Method used to handle the pole singularities."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('filter', None),
            ('pole rotation', None),
            ('artificial island', None),
            ('none', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: dynamical core: diffusion horizontal
def _atmos_dynamical_core_diffusion_horizontal():
    """Horizontal diffusion scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_name ', '1.1',
             'horizontal diffusion scheme name'),
            ('selection', 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_method', '1.1',
             'horizontal diffusion scheme method'), ],
        'constraints': [
            [('heading', 'value', 'Horizontal diffusion scheme name'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_name.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_name', '1.1'), ],
            [('heading', 'value', 'Horizontal diffusion scheme method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_method.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_diffusion_horizontal_scheme_method', '1.1'), ],
             ]
    }


# TODO Look at cmip5q responses to create enumeration for this
def _atmos_dynamical_core_diffusion_horizontal_scheme_name():
    """Commonly used name for the horizontal diffusion scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('???', None),
            ('other', None),
        ]
    }


# TODO Review the cmip5q responses to extend this enumeration
def _atmos_dynamical_core_diffusion_horizontal_scheme_method():
    """Numerical method used by the horizontal diffusion scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('iterated Laplacian', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: dynamical core: advection tracers
# TODO Look at cmip5q to provide enumeration for conserved quantities
def _atmos_dynamical_core_advection_tracers():
    """Tracer advection scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_advection_tracers_scheme_name ', '1.1',
             'tracer advection scheme name'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_tracers_scheme_characteristics', '1.N',
             'tracer advection scheme characteristics'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_tracers_conserved_quantities', '1.N',
             'tracer advection scheme conserved quantities'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_tracers_conservation_method', '1.1',
             'tracer advection scheme conservation method'), ],
        'constraints': [
            [('heading', 'value', 'Tracer advection scheme name'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_tracers_scheme_name.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_tracers_scheme_name', '1.1'), ],
            [('heading', 'value', 'Tracer advection scheme characteristics'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_tracers_scheme_characteristics.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_tracers_scheme_characteristics', '1.N'), ],
            [('heading', 'value', 'Tracer advection scheme conserved quantities'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_tracers_conserved_quantities.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_tracers_conserved_quantities', '1.N'), ],
            [('heading', 'value', 'Tracer advection scheme conservation method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_tracers_conservation_method.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_tracers_conservation_method', '1.1'), ],
             ]
    }


def _atmos_dynamical_core_advection_tracers_scheme_name():
    """Commonly used name for the tracer advection scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Heun', None),
            ('Roe and VanLeer', None),
            ('Roe and Superbee', None),
            ('Prather', None),
            ('UTOPIA', None),
            ('other', None),
        ]
    }


def _atmos_dynamical_core_advection_tracers_scheme_characteistics():
    """Characteristics of the numerical scheme used for the advection of tracers."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Eulerian', None),
            ('modified Euler', None),
            ('Lagrangian', None),
            ('semi-Lagrangian', None),
            ('cubic semi-Lagrangian', None),
            ('quintic semi-Lagrangian', None),
            ('mass-conserving', None),
            ('finite volume', None),
            ('flux-corrected', None),
            ('linear', None),
            ('quadratic', None),
            ('quartic', None),
            ('other', None),
        ]
    }


# TODO Review cmip5q responses to provide enumeration for this.
def _atmos_dynamical_core_advection_tracers_conserved_quantities():
    """Quantities conserved through the tracers advection scheme.."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('???', None),
            ('other', None),
        ]
    }


def _atmos_dynamical_core_advection_tracers_conservation_method():
    """Method used to ensure conservation in the tracers advection scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('conservation fixer', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: dynamical core: advection momentum
# TODO Look at cmip5q to provide enumeration for conserved quantities
def _atmos_dynamical_core_advection_momentum():
    """Momentum advection scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_advection_momentum_scheme_name ', '1.1',
             'momentum advection schemes name'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_momentum_scheme_characteristics', '1.N',
             'momentum advection scheme characteristics'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_momentum_scheme_staggering_type', '1.1',
             'momentum advection scheme staggering type'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_momentum_conserved_quantities', '1.N',
             'momentum advection scheme conserved quantities'),
            ('selection', 'cmip6.atmos_dynamical_core_advection_momentum_conservation_method', '1.1',
             'momentum advection scheme conservation method'), ],
        'constraints': [
            [('heading', 'value', 'Momentum advection scheme name'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_name.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_name', '1.1'), ],
            [('heading', 'value', 'Momentum advection scheme characteristics'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_characteristics.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_characteristics', '1.N'), ],
            [('heading', 'value', 'Momentum advection scheme staggering type'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_staggering_type.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_momentum_scheme_staggering_type', '1.1'), ],
            [('heading', 'value', 'Momentum advection scheme conserved quantities'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_momentum_conserved_quantities.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_momentum_conserved_quantities', '1.N'), ],
            [('heading', 'value', 'Momentum advection scheme conservation method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_dynamical_core_advection_momentum_conservation_method.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_dynamical_core_advection_momentum_conservation_method', '1.1'), ],
             ]
    }


def _atmos_dynamical_core_advection_momentum_scheme_name():
    """Commonly used name for the momentum advection scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('VanLeer', None),
            ('Janjic', None),
            ('SUPG (Streamline Upwind Petrov-Galerkin)', None),
            ('other', None),
        ]
    }


def _atmos_dynamical_core_advection_momentum_scheme_characteistics():
    """Characteristics of the numerical scheme used for the advection of momentum."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('2nd order', None),
            ('4th order', None),
            ('cell-centred', None),
            ('staggered grid', None),
            ('semi-staggered grid', None),
            ('other', None),
        ]
    }


# TODO allow to be blank if the _atmos_dynamical_core_advection_mementum_scheme_characteristics is not 'staggered'
def _atmos_dynamical_core_advection_momentum_scheme_staggering_type():
    """If scheme characteristics specify staggered grid, describe the type of staggering."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Arakawa B-grid', None),
            ('Arakawa C-grid', None),
            ('Arakawa D-grid', None),
            ('Arakawa E-grid', None),
            ('other', None),
        ]
    }


# TODO Review cmip5q responses to provide enumeration for this.
def _atmos_dynamical_core_advection_momentum_conserved_quantities():
    """Quantities conserved through the tracers advection scheme.."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ("Angular momentum", None),
            ("Horizontal momentum", None),
            ("Enstrophy", None),
            ("Mass", None),
            ("Total energy", None),
            ("Vorticity", None),
            ('Other', None)
        ]
    }


# TODO Reveiw cmip5q responses to extend enumeration for this.
def _atmos_dynamical_core_advection_momentum_conservation_method():
    """Method used to ensure conservation in the tracers advection scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('conservation fixer', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: dynamical core: top boundary
# TODO configure the class to allow the user to enter their own text description
def _atmos_dynamical_core_top_boundary():
    """Type of boundary layer at the top of the model."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_top_boundary_condition', '1.1', 'top boundary condition'),
            ('string', 'cmip6.atmos_dynamical_core_top_heat', '1.1', 'top boundary heat treatment'),
            ('string', 'cmip6.atmos_dynamical_core_top_wind', '1.1', 'top boundary wind treatment'), ],
        'constraints': [
            [('heading', 'value', 'Top boundary condition'),
                ('vocabulary', 'value', 'cmip6.atmos_dynamical_core_top_boundary_condition.%s' % version),
                ('selection', 'from', 'cmip6.atmos_dynamical_core_top_boundary_condition', '1.1'), ],
            [('heading', 'value', 'Top boundary heat treatment'), ],
            [('heading', 'value', 'Top boundary wind treatment'), ], ]
    }


def _atmos_dynamical_core_top_boundary_condition():
    """Type of boundary layer at the top of the model."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('sponge layer', None),
            ('radiation boundary condition', None),
            ('other', None),
        ]
    }


# TODO configure the class to allow the user to enter a text description
def _atmos_dynamical_core_top_heat():
    """Description of any specific treatment of heat at the top of the model."""
    return{
    }


# TODO configure the class to allow the user to enter a text description
def _atmos_dynamical_core_top_wind():
    """Description of any specific treatment of wind at the top of the model."""
    return{
    }


# Process-detail: Atmosphere: dynamical core: lateral boundary
def _atmos_dynamical_core_lateral_boundary():
    """Type of lateral boundary condition (if the model is a regional model)."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_lateral_boundary',
             '1.1', 'lateral boundary condition'), ],
        'constraints': [
            ('heading', 'value', 'Lateral boundary condition'),
            ('vocabulary', 'value', 'cmip6.atmos_dynamical_core_lateral_boundary_attribute.%s' % version),
            ('selection', 'from', 'cmip6.atmos_dynamical_core_lateral_boundary_attribute', '1.1'), ]
    }


def _atmos_dynamical_core_lateral_boundary_attribute():
    """Type of lateral boundary condition (if the model is a regional model)."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('sponge layer', None),
            ('radiation boundary condition', None),
            ('none', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: dynamical core: prognostic variables
def _atmos_dynamical_core_prognostic_variables():
    """List of the model prognostic variables."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_dynamical_core_prognostic_variables', '1.N', 'prognostic variables'), ],
        'constraints': [
            ('heading', 'value', 'Prognostic variables'),
            ('vocabulary', 'value', 'cmip6.atmos_dynamical_core_prognostic_variables_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_dynamical_core_prognostic_variables_attributes', '1.N'), ]
    }


def _atmos_dynamical_core_prognostic_variables_attributes():
    """List of the model prognostic variables."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('surface pressure', None),
            ('wind components', None),
            ('divergence/curl', None),
            ('temperature', None),
            ('potential temperature', None),
            ('total water', None),
            ('water vapour', None),
            ('water liquid', None),
            ('water ice', None),
            ('total water moments', None),
            ('clouds', None),
            ('radiation', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: radiation: longwave
# TODO configure the class to allow the user to enter an integer
def _atmos_radiation_longwave_scheme():
    """Longwave radiation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_longwave_scheme_type', '1.1', 'longwave radiation scheme type'),
            ('selection', 'cmip6.atmos_radiation_longwave_scheme_method', '1.1', 'longwave radiation scheme method'),
            ('integer', 'cmip6.atmos_radiation_longwave_scheme_spectral_intervals', '1.1',
             'longwave radiation scheme spectral intervals'), ],
        'constraints': [
            [('heading', 'value', 'longwave radiation scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_radiation_longwave_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_radiation_longwave_scheme_type', '1.1'), ],
            [('heading', 'value', 'longwave radiation scheme method'),
                ('vocabulary', 'value', 'cmip6.atmos_radiation_longwave_scheme_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_radiation_longwave_scheme_method', '1.1'), ],
            [('heading', 'value', 'longwave radiation scheme spectral intervals'), ], ]
    }


def _atmos_radiation_longwave_scheme_type():
    """Type of scheme used for longwave radiation parameterisation."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('wide-band model', None),
            ('wide-band model (Morcrette)', None),
            ('K-correlated', None),
            ('K-correlated (RRTM)', None),
            ('other', None),
        ]
    }


def _atmos_radiation_longwave_scheme_method():
    """Method for the radiative transfer calculations used in the longwave scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('two-stream', None),
            ('layer interaction', None),
            ('other', None),
        ]
    }


# TODO configure the class to allow the user to enter their own integer
def _atmos_radiation_longwave_scheme_spectral_intervals():
    """Number of spectral intervals of the longwave radiation scheme."""
    return{
    }


# Process-detail: Atmosphere: radiation: shortwave
# TODO configure the class to allow the user to enter their own integer
def _atmos_radiation_shortwave_scheme():
    """Shortwave radiation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_shortwave_scheme_type',
             '1.1', 'shortwave radiation scheme type'),
            ('integer', 'cmip6.atmos_radiation_shortwave_scheme_spectral_intervals',
             '1.1', 'shortwave radiation scheme spectral intervals'), ],
        'constraints': [
            [('heading', 'value', 'shortwave radiation scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_radiation_shortwave_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_radiation_shortwave_scheme_type', '1.1'), ],
            [('heading', 'value', 'shortwave radiation scheme spectral intervals'), ], ]
    }


def _atmos_radiation_shortwave_scheme_type():
    """Type of scheme used for shortwave radiation parameterisation."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('wide-band model', None),
            ('wide-band model (Fouquart)', None),
            ('other', None),
        ]
    }


# TODO configure the class to allow the user to enter an integer
def _atmos_radiation_shortwave_scheme_spectral_intervals():
    """Number of spectral intervals of the shortwave radiation scheme."""
    return{
    }


# Process-detail: Atmosphere: radiation: time step
# TODO configure the class to allow the user to enter an integer
def _atmos_radiation_scheme_timestep():
    """Time step of hte radiation scheme (seconds)."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('integer', 'cmip6.atmos_radiation_scheme_timestep', '1.1', 'radiation scheme timestep (seconds)'), ],
        'constraints': [
            ('heading', 'value', 'Radiation scheme timestep (seconds)'), ]
    }


# TODO configure the class to allow the user to enter their own integer
def _atmos_radiation_scheme_timestep_attribute():
    """Time step of the radiation scheme (seconds)."""
    return{
    }


# Process-detail: Atmosphere: radiation: aerosol types
def _atmos_radiation_aerosol_types():
    """Types of aerosols whose radiative effect is taken into account in the atmospheric model."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_aerosol_types_attributes', '1.N', 'radiative aerosols'), ],
        'constraints': [
            ('heading', 'value', 'Radiative aerosols'),
            ('vocabulary', 'value', 'cmip6.atmos_radiation_aerosol_types_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_radiation_aerosol_types_attributes', '1.N'), ]
    }


def _atmos_radiation_aerosol_types_attributes():
    """Types of aerosols whose radiative effect is taken into account in the atmospheric model."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('sulphate', None),
            ('nitrate', None),
            ('sea salt', None),
            ('dust', None),
            ('ice', None),
            ('organic', None),
            ('BC (black carbon / soot)', None),
            ('SOA (secondary organic aerosols)', None),
            ('POM (particulate organic matter)', None),
            ('polar stratospheric ice', None),
            ('NAT (nitric acid trihydrate)', None),
            ('NAD (nitric acid dihydrate)', None),
            ('STS (supercooled ternary solution aerosol particle)', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: radiation: ghg types
def _atmos_radiation_ghg_types():
    """Types of greenhouse gases whose radiative effect is taken into account in the atmospheric model."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_ghg_types_attributes', '1.N', 'radiative greenhouse gases'), ],
        'constraints': [
            ('heading', 'value', 'Radiative greenhouse gases'),
            ('vocabulary', 'value', 'cmip6.atmos_radiation_ghg_types_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_radiation_ghg_types_attributes', '1.N'), ]
    }


def _atmos_radiation_ghg_types_attributes():
    """Types of greenhouse gases whose radiative effect is taken into account in the atmospheric model."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('CO2', None),
            ('CH4', None),
            ('N2O', None),
            ('CFC', None),
            ('H2O', None),
            ('O3', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: radiation: cloud ice radiative properties
# TODO We need to come up with an enumeration for this!
def _atmos_radiation_cloud_ice():
    """Radiative properties of ice crystals in clouds."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_cloud_ice_properties', '1.N', 'radiative properties of cloud ice'), ],
        'constraints': [
            ('heading', 'value', 'Radiative properties of cloud ice'),
            ('vocabulary', 'value', 'cmip6.atmos_radiation_cloud_ice_properties.%s' % version),
            ('selection', 'from', 'cmip6.atmos_radiation_cloud_ice_properties', '1.N'), ]
    }


# TODO We need to come up with an enumeration for this!
def _atmos_radiation_cloud_ice_properties():
    """Radiative properties of ice crystals in clouds."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('???', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: radiation: cloud liquid radiative properties
# TODO We need to come up with an enumeration for this!
def _atmos_radiation_cloud_liquid():
    """Radiative properties of liquid droplets in clouds."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_radiation_cloud_liquid_properties', '1.N',
             'radiative properties of cloud droplets'), ],
        'constraints': [
            ('heading', 'value', 'Radiative properties of cloud droplets'),
            ('vocabulary', 'value', 'cmip6.atmos_radiation_cloud_liquid_properties.%s' % version),
            ('selection', 'from', 'cmip6.atmos_radiation_cloud_liquid_properties', '1.N'), ]
    }


# TODO We need to come up with an enumeration for this!
def _atmos_radiation_cloud_liquid_properties():
    """Radiative properties of liquid droplets in clouds."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('???', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: turbulence and convection: boundary layer turbulence
# TODO configure the class to allow the user to enter their own integer
def _atmos_boundary_layer_turbulence_scheme():
    """Boundary layer turbulence scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_boundary_layer_turbulence_scheme_name',
             '1.1', 'boundary layer turbulence scheme name'),
            ('integer', 'cmip6.atmos_boundary_layer_turbulence_scheme_closure_order',
             '1.1', 'boundary layer turbulence scheme closure order'),
            ('selection', 'cmip6.atmos_boundary_layer_turbulence_scheme_type',
             '1.1', 'boundary layer turbulence scheme type'),
            ('boolean', 'cmip6.atmos_boundary_layer_turbulence_scheme_counter_gradient',
             '1.1', 'uses boundary layer turbulence scheme counter gradient'), ],
        'constraints': [
            [('heading', 'value', 'Boundary layer turbulence scheme name'),
                ('vocabulary', 'value', 'cmip6.atmos_boundary_layer_turbulence_scheme_name.%s' % version),
                ('selection', 'from', 'cmip6.atmos_boundary_layer_turbulence_scheme_name', '1.1'), ],
            [('heading', 'value', 'Boundary layer turbulence scheme closure order'), ],
            [('heading', 'value', 'Boundary layer turbulence scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_boundary_layer_turbulence_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_boundary_layer_turbulence_scheme_type', '1.1'), ],
            [('heading', 'value', 'Uses boundary layer turbulence scheme counter gradient'), ], ]
    }


def _atmos_boundary_layer_turbulence_scheme_name():
    """Commonly used name for the boundary layer turbulence scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Mellor-Yamada', None),
            ('other', None),
        ]
    }


# TODO allow to be blank if the _atmos_boundary_layer_turbulence_scheme_name is not 'Mellor_Yamada'
# TODO configure the class to allow the user to enter an integer
def _atmos_boundary_layer_turbulence_scheme_closure_order():
    """Closure order of the Mellor-Yamada scheme (if used)."""
    return{
    }


def _atmos_boundary_layer_turbulence_scheme_type():
    """Type of scheme used for the parameterisation of turbulence in the boundary layer."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('TKE prognostic', None),
            ('TKE diagnostic', None),
            ('TKE coupled with water', None),
            ('vertical profile of Kz', None),
            ('other', None),
        ]
    }


# TODO configure this to allow the user to enter a boolean item
def _atmos_boundary_layer_turbulence_scheme_counter_gradient():
    """Application of a counter-gradient term for calculation of the vertical turbulent heat fluxes in case of slightly
    stable layer; counter-gradient heat flux for parameterisation of the non-local vertical diffusion."""
    return{
    }


# Process-detail: Atmosphere: turbulence and convection: deep convection
# TODO configure the class to allow the user to enter their own string
def _atmos_deep_convection_scheme():
    """Deep convection scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('string', 'cmip6.atmos_deep_convection_scheme_name',
             '1.1', 'deep convection scheme name'),
            ('selection', 'cmip6.atmos_deep_convection_scheme_type',
             '1.1', 'deep convection scheme type'),
            ('selection', 'cmip6.atmos_deep_convection_scheme_method',
             '1.N', 'deep convection scheme method'), ],
        'constraints': [
            [('heading', 'value', 'deep convection scheme name'), ],
            [('heading', 'value', 'deep convection scheme type'),
                ('vocabulary', 'value',
                 'cmip6.atmos_deep_convection_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_deep_convection_scheme_type', '1.1'), ],
            [('heading', 'value', 'deep convection scheme method'),
                ('vocabulary', 'value',
                 'cmip6.atmos_deep_convection_scheme_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_deep_convection_scheme_method', '1.N'), ], ]
    }


# TODO configure the class to allow the user to enter a string
def _atmos_deep_convection_scheme_name():
    """Commonly used name for the deep convection scheme."""
    return{
    }


def _atmos_deep_convection_scheme_type():
    """Type of scheme used for the parameterisation of deep convection."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('mass-flux', None),
            ('adjustment', None),
            ('other', None),
        ]
    }


# TODO allow to be blank if the _atmos_deep_convection_scheme_type is not 'mass_flux'
def _atmos_deep_convection_scheme_method():
    """If deep convection uses a mass-flux scheme enter the method used."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('CAPE', 'Mass flux determined by CAPE'),
            ('bulk', 'A bulk mass flux scheme is used'),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: turbulence and convection: deep convection processes
def _atmos_deep_convection_scheme_processes():
    """Physical processes taken into account in the parameterisation of deep convection."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_deep_convection_scheme_processes_attributes',
             '1.N', 'deep convection scheme processes'), ],
        'constraints': [
            ('heading', 'value', 'Deep convection scheme processes'),
            ('vocabulary', 'value', 'cmip6.atmos_deep_convection_scheme_processes_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_deep_convection_scheme_processes_attributes', '1.N'), ]
    }


def _atmos_deep_convection_scheme_processes_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('vertical momentum transport', None),
            ('convective momentum transport', None),
            ('entrainment', None),
            ('detrainment', None),
            ('penetrative convection', None),
            ('updrafts and downdrafts', None),
            ('radiative effect of anvils', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: turbulence and convection: shallow convection
# TODO configure the class to allow the user to enter their own string
def _atmos_shallow_convection_scheme():
    """Shallow convection scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_shallow_convection_scheme_method',
             '1.1', 'shallow convection scheme method'),
            ('string', 'cmip6.atmos_shallow_convection_scheme_name',
             '1.1', 'shallow convection scheme name'),
            ('selection', 'cmip6.atmos_shallow_convection_scheme_type',
             '1.1', 'shallow convection scheme type'), ],
        'constraints': [
            [('heading', 'value', 'shallow convection scheme method'),
                ('vocabulary', 'value', 'cmip6.atmos_shallow_convection_scheme_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_shallow_convection_scheme_method', '1.1'), ],
            [('heading', 'value', 'shallow convection scheme name'), ],
            [('heading', 'value', 'shallow convection scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_shallow_convection_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_shallow_convection_scheme_type', '1.1'), ], ]
    }


def _atmos_shallow_convection_scheme_method():
    """Method used for shallow convection."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('same as deep (unified)', None),
            ('included in boundary layer turbulence', None),
            ('separated', None),
        ]
    }


# TODO allow to be blank if _atmos_shallow_convection_scheme_method is not 'separated'
# TODO configure the class to allow the user to enter a string
def _atmos_shallow_convection_scheme_name():
    """Commonly used name for the shallow convection scheme."""
    return{
    }


# TODO allow to be blank if _atmos_shallow_convection_scheme_method is not 'separated'
def _atmos_shallow_convection_scheme_type():
    """Type of scheme used for the parameterisation of shallow convection."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('mass-flux', None),
            ('other', None),
            ('none', None),
        ]
    }


# Process-detail: Atmosphere: turbulence and convection: shallow convection processes
def _atmos_shallow_convection_scheme_processes():
    """Physical processes taken into account in the parameterisation of shallow convection."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_shallow_convection_scheme_processes_attributes',
             '1.N', 'shallow convection scheme processes'), ],
        'constraints': [
            ('heading', 'value', 'Shallow convection scheme processes'),
            ('vocabulary', 'value', 'cmip6.atmos_shallow_convection_scheme_processes_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_shallow_convection_scheme_processes_attributes', '1.N'), ]
    }


def _atmos_shallow_convection_scheme_processes_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('vertical momentum transport', None),
            ('convective momentum transport', None),
            ('entrainment', None),
            ('detrainment', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: turbulence and convection: other convection
# TODO configure the class to allow the user to enter a string
def _atmos_other_convection_scheme():
    """Other convection scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('string', 'cmip6.atmos_other_convection_scheme_name', '1.1', 'other convection scheme name'),
            ('selection', 'cmip6.atmos_other_convection_scheme_type', '1.1', 'other convection scheme type'), ],
        'constraints': [
            [('heading', 'value', 'other convection scheme name'), ],
            [('heading', 'value', 'other convection scheme type'),
                ('vocabulary', 'value', 'cmip6.atmos_other_convection_scheme_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_other_convection_scheme_type', '1.1'), ], ]
    }


# TODO configure the class to allow the user to enter their own string
def _atmos_other_convection_scheme_name():
    """Commonly used name for other convection scheme type."""
    return{
    }


def _atmos_other_convection_scheme_type():
    """Type of scheme used for the parameterisation of other convection."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('mass-flux', None),
            ('other', None),
        ]
    }


# Process Detail: Atmosphere: microphysics and precipitation: large scale precipitation scheme
# TODO format this class to allow users to enter a string
def _atmos_large_scale_precipitation_scheme():
    """Commonly used name of the large scale precipitation parameterisation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('string', 'cmip6.atmos_large_scale_precipitation_scheme_name', '1.1',
             'large scale precipitation scheme'), ],
        'constraints': [
            ('heading', 'value', 'Large scale precipitation scheme name'), ]
    }


# TODO format this class to allow users to enter a string
def _atmos_large_scale_precipitation_scheme_name():
    """Commonly used name of the large scale precipitation parameterisation scheme."""
    return{
    }


# Process Detail: Atmosphere: microphysics and precipitation: precipitating hydrometeors
def _atmos_large_scale_precipitation_hydrometeors():
    """Precipitating hydrometeors taken into account in the large scale precipitation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_large_scale_precipitation_hydrometeor_types', '1.N',
             'large scale precipitation hydrometeors'), ],
        'constraints': [
            ('heading', 'value', 'Large scale precipitation hydrometeors'),
            ('vocabulary', 'value', 'cmip6.atmos_large_scale_precipitation_hydrometeor_types.%s' % version),
            ('selection', 'from', 'cmip6.atmos_large_scale_precipitation_hydrometeor_types', '1.N'), ]
    }


def _atmos_large_scale_precipitation_hydrometeor_types():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('liquid rain', None),
            ('snow', None),
            ('hail', None),
            ('graupel', None),
            ('other', None),
        ]
    }


# Process Detail: Atmosphere: microphysics and precipitation: microphysics scheme
# TODO format this class to allow users to enter a string
def _atmos_cloud_microphysics_scheme():
    """Commonly used name of the microphysics parameterisation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('string', 'cmip6.atmos_cloud_microphysics_scheme_name', '1.1', 'cloud microphysics scheme'), ],
        'constraints': [
            ('heading', 'value', 'Cloud microphysics scheme name'), ]
    }


# TODO format this to allow the user to enter a string
def _atmos_large_scale_microphysics_scheme_name():
    """Commonly used name of the microphysics parameterisation scheme."""
    return{
    }


# Process Detail: Atmosphere: microphysics and precipitation: microphysics processes
def _atmos_cloud_microphysics_processes():
    """Precipitating hydrometeors taken into account in the large scale precipitation scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_microphysics_processes_attributes', '1.N',
             'cloud microphysics processes'), ],
        'constraints': [
            ('heading', 'value', 'Cloud microphysics processes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_microphysics_processes_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_microphysics_processes_attributes', '1.N'), ]
    }


def _atmos_cloud_microphysics_processes_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('mixed phase', None),
            ('cloud droplets', None),
            ('cloud ice', None),
            ('ice nucleation', None),
            ('water vapour deposition', None),
            ('effect of raindrops', None),
            ('effect of snow', None),
            ('effect of graupel', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud scheme: separate treatment
def _atmos_cloud_scheme_separate_treatment():
    """Different cloud schemes for the different types of clouds (convective, stratiform and boundary layer clouds)."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('boolean', 'cmip6.atmos_cloud_scheme_uses_separate_treatment', '1.1',
             'Separate schemes for different cloud types'), ],
        'constraints': [
            ('heading', 'value', 'Separate cloud schemes for different cloud types'), ]
    }


# TODO configure to allow user to enter boolean input
def _atmos_cloud_scheme_uses_separate_treatment():
    return{
    }


# Process-detail: Atmosphere: cloud scheme: cloud overlap
def _atmos_cloud_scheme_cloud_overlap():
    """Method for taking into account overlapping of cloud layers."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_cloud_overlap_method', '1.1',
             'cloud scheme cloud overlap method'), ],
        'constraints': [
            ('heading', 'value', 'Cloud scheme cloud overlap method'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_scheme_cloud_overlap_method.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_scheme_cloud_overlap_method', '1.1'), ]
    }


def _atmos_cloud_scheme_cloud_overlap_method():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('random', None),
            ('none', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud scheme: cloud processes
def _atmos_cloud_scheme_processes():
    """Cloud processes included in the cloud scheme."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_processes_attributes', '1.N', 'cloud scheme processes'), ],
        'constraints': [
            ('heading', 'value', 'Cloud scheme cloud processes'),
            ('vocabulary', 'value', 'cmip6.atmos_cloud_scheme_processes_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_cloud_scheme_processes_attributes', '1.N'), ]
    }


def _atmos_cloud_scheme_processes_attributes():
    """Processes included in the cloud scheme."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('entrainment', None),
            ('detrainment', None),
            ('bulk cloud', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud scheme: sub-grid scale water distribution
# TODO configure the class to allow the user to enter their own string/integer
def _atmos_cloud_scheme_sub_grid_scale_water_distribution():
    """Sub-grid scale water distribution."""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type',
             '1.1', 'sub-grid scale water distribution type'),
            ('string', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name',
             '1.1', 'sub-grid scale water distribution function name'),
            ('integer', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order',
             '1.1', 'sub-grid scale water distribution function type'),
            ('selection', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection',
             '1.N', 'sub-grid scale water distribution coupling with convection'), ],
        'constraints': [
            [('heading', 'value', 'sub-grid scale water distribution type attribute'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_type',
                 '1.1'), ],
            [('heading', 'value', 'sub-grid scale water distribution function name'), ],
            [('heading', 'value', 'sub-grid scale water distribution function order'), ],
            [('heading', 'value', 'sub-grid scale water distribution coupling with convection'),
                ('vocabulary', 'value',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection.%s' % version),
                ('selection', 'from',
                 'cmip6.atmos_cloud_scheme_sub_grid_scale_water_distribution_convection', '1.N'), ], ]
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_type():
    """Approach used for cloud water content and fractional cloud cover"""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('prognostic', None),
            ('diagnostic', None),
        ]
    }


# TODO configure the class to allow the user to enter their own string
def _atmos_cloud_scheme_sub_grid_scale_water_distribution_function_name():
    """Commonly used name of the probability density function (PDF)
    representing the distribution of water vapour within a grid box."""
    return{
    }


# TODO configure the class to allow the user to enter their own integer
def _atmos_cloud_scheme_sub_grid_scale_water_distribution_function_order():
    """Order of the function (PDF) used to represent subgrid scale water vapour distribution."""
    return{
    }


def _atmos_cloud_scheme_sub_grid_scale_water_distribution_convection():
    """Type(s) of convection that the formation of clouds is coupled with."""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('coupled with deep', None),
            ('coupled with shallow', None),
            ('not coupled with convection', None),
        ]
    }


# Process-detail: Atmosphere: cloud simulator: COSP attributes
# TODO configure the class to allow the user to enter an integer
def _atmos_cloud_simulator_cosp_attributes():
    """CFMIP Observational Simulator Package attributes"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_cosp_run_configuration', '1.1',
             'cloud simulator COSP run configuration'),
            ('integer', 'cmip6.atmos_cloud_simulator_cosp_number_of_grid_points', '1.1',
             'cloud simulator COSP number of grid points'),
            ('integer', 'cmip6.atmos_cloud_simulator_cosp_number_of_columns', '1.1',
             'cloud simulator COSP number of cloumns'),
            ('integer', 'cmip6.atmos_cloud_simulator_cosp_number_of_levels', '1.1',
             'cloud simulator COSP number of levels'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator COSP run configuration'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_cosp_run_configuration.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_cosp_run_configuration', '1.1'), ],
            [('heading', 'value', 'Cloud simulator COSP number of grid points'), ],
            [('heading', 'value', 'Cloud simulator COSP number of columns'), ],
            [('heading', 'value', 'Cloud simulator COSP number of levels'), ], ]
    }


def _atmos_cloud_simulator_cosp_run_configuration():
    """Method used to run the CFMIP Observational Simulator Package"""
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('Inline', None),
            ('Offline', None),
            ('None', None),
        ]
    }


# TODO configure the class to allow the user to enter an integer
def _atmos_cloud_simulator_cosp_number_of_grid_points():
    """Number of grid points used by COSP."""
    return{
    }


# TODO configure the class to allow the user to enter an integer
def _atmos_cloud_simulator_cosp_number_of_columns():
    """Number of subcolumns used by COSP."""
    return{
    }


# TODO configure the class to allow the user to enter an integer
def _atmos_cloud_simulator_cosp_number_of_levels():
    """Number of model levels used by COSP."""
    return{
    }


# Process-detail: Atmosphere: cloud simulator: radar simulator
# TODO configure the class to allow the user to enter a real number
def _atmos_cloud_simulator_inputs_radar():
    """Characteristics of the cloud radar simulator"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('real', 'cmip6.atmos_cloud_simulator_inputs_radar_frequency', '1.1',
             'cloud simulator radar frequency'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_radar_type', '1.1',
             'cloud simulator radar type'),
            ('boolean', 'cmip6.atmos_cloud_simulator_inputs_radar_gas_absorption', '1.1',
             'cloud simulator radar uses gas absorption'),
            ('boolean', 'cmip6.atmos_cloud_simulator_inputs_radar_effective_radius', '1.1',
             'cloud simulator radar uses effective radius'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator radar frequency (GHz)'), ],
            [('heading', 'value', 'Cloud simulator radar type'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_radar_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_radar_type', '1.1'), ],
            [('heading', 'value', 'Cloud simulator radar uses gas absorption'), ],
            [('heading', 'value', 'Cloud simulator radar uses effective radius'), ],
        ]
    }


# TODO configure to allow the user to enter a real number
def _atmos_cloud_simulator_inputs_radar_frequency():
    """CloudSat radar frequency (GHz)."""
    return{
    }


def _atmos_cloud_simulator_inputs_radar_type():
    """Type of radar."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('surface', None),
            ('space borne', None),
        ]
    }


# TODO configure to allow user to enter boolean input
def _atmos_cloud_simulator_inputs_radar_gas_absorption():
    """Does the radar simulator include gaseous absorption?"""
    return {
    }


# TODO configure to allow user to enter boolean input
def _atmos_cloud_simulator_inputs_radar_efective_radius():
    """Does the radar simulator use effective radius?"""
    return {
    }


# Process-detail: Atmosphere: cloud simulator: lidar simulator
def _atmos_cloud_simulator_inputs_lidar():
    """Characteristics of the cloud lidar simulator"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type', '1.1', 'cloud simulator lidar ice type'),
            ('selection', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap', '1.N', 'cloud simulator lidar overlap'),
        ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator lidar ice type'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_lidar_ice_type', '1.1'), ],
            [('heading', 'value', 'Cloud simulator lidar overlap'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_inputs_lidar_overlap', '1.N'), ], ]
    }


def _atmos_cloud_simulator_inputs_lidar_ice_type():
    """Ice particle shape in lidar calculations"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('ice spheres', None),
            ('ice non-spherical', None),
        ]
    }


def _atmos_cloud_simulator_inputs_lidar_overlap():
    """lidar overlap type"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('max', None),
            ('random', None),
            ('other', None),
        ]
    }


# Process-detail: Atmosphere: cloud simulator: ISSCP attributes        
def _atmos_cloud_simulator_isscp_attributes():
    """ISSCP Characteristics"""
    return{
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_cloud_simulator_isscp_top_height', '1.N',
             'cloud simulator ISSCP top height'),
            ('selection', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction', '1.1',
             'cloud simulator ISSCP top height direction'), ],
        'constraints': [
            [('heading', 'value', 'Cloud simulator ISSCP top height'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_isscp_top_height.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_isscp_top_height', '1.N'), ],
            [('heading', 'value', 'Cloud simulator ISSCP top height direction'),
                ('vocabulary', 'value', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction.%s' % version),
                ('selection', 'from', 'cmip6.atmos_cloud_simulator_isscp_top_height_direction', '1.1'), ], ]
    }


def _atmos_cloud_simulator_isscp_top_height():
    """Cloud top height management"""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('no adjustment', None),
            ('IR brightness', None),
            ('visible optical depth', None),
        ]
    }


def _atmos_cloud_simulator_isscp_top_height_direction():
    """Direction for finding the radiance determined cloud-top pressure. 
       Atmosphere pressure level with interpolated temperature equal to the radiance determined 
       cloud-top pressure."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('lowest altitude level', None),
            ('highest altitude level', None),
        ]
    }


# Process-detail: Atmosphere: gravity waves: sponge layer
# May prove to be candidate for deprecation if top of atmos sponge layer turns out to
# already be covered by the atmos dynamical core process class
def _atmos_gravity_waves_sponge_layer():
    """Sponge layer in the upper levels in order to avoid gravity wave reflection at the top."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_sponge_layer_attributes', '1.1', 'gravity waves sponge layer'), ],
        'constraints': [
            ('heading', 'value', 'Gravity wave sponge layer attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_sponge_layer_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_sponge_layer_attributes', '1.1'), ]
    }


def _atmos_gravity_waves_sponge_layer_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('None', None),
            ('Other', None),
        ]
    }


# Process-detail: Atmosphere: gravity waves: background distribution of waves
def _atmos_gravity_waves_background():
    """Background distribution of waves. """
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_background_attributes', '1.1', 'background wave distribution'), ],
        'constraints': [
            ('heading', 'value', 'Background wave distribution attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_background_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_background_attributes', '1.1'), ]
    }


def _atmos_gravity_waves_background_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('None', None),
            ('Other', None),
        ]
    }

# Process-detail: Atmosphere: gravity waves: subgrid-scale orography
def _atmos_gravity_waves_subgrid_scale_orography():
    """Subgrid scale orography effects taken into account."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.detail',
        'properties': [
            ('selection', 'cmip6.atmos_gravity_waves_subgrid_scale_orography_attributes', '1.N',
             'subgrid-scale orography'), ],
        'constraints': [
            ('heading', 'value', 'Sub-grid scale orography attributes'),
            ('vocabulary', 'value', 'cmip6.atmos_gravity_waves_subgrid_scale_orography_attributes.%s' % version),
            ('selection', 'from', 'cmip6.atmos_gravity_waves_subgrid_scale_orography_attributes', '1.N'), ]
    }


def _atmos_gravity_waves_subgrid_scale_orography_attributes():
    return{
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('effect on drag', None),
            ('effect on lifting', None),
            ('Other', None),
        ]
    }


# Algorithm: Atmosphere: gravity waves: orographic gravity waves
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: source mechanisms
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: calculation method
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: propagation scheme
# Process-detail: Atmosphere: gravity waves: orographic gravity waves: dissipation scheme
def _atmos_orographic_gravity_waves():
    """Gravity waves generated due to the presence of orography."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.algorithm',
        'properties':
        [
            ('selection', 'cmip6.atmos_orographic_gravity_waves_source_mechanism',
                '1.N', 'Orographic gravity wave source mechanisms'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_calculation_method',
                '1.N', 'Orographic gravity wave calculation method'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_propagation_scheme',
                '1.1', 'Orographic gravity wave propogation scheme'),
            ('selection', 'cmip6.atmos_orographic_gravity_waves_dissipation_scheme',
                '1.1', 'Orographic gravity wave dissipation scheme'),
        ],
        'constraints': [
            [('heading', 'value', 'Orographic gravity wave source mechanisms'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_source_mechanisms.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_source_mechanisms', '1.N'), ],
            [('heading', 'value', 'Orographic gravity wave calculation method'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_calculation_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_calculation_mehtod', '1.N'), ],
            [('heading', 'value', 'Orographic gravity wave propogation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_propagation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_propagation_scheme', '1.1'), ],
            [('heading', 'value', 'Orographic gravity wave dissipation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_orographic_gravity_wave_dissipation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_orographic_gravity_wave_dissipation_scheme', '1.1'), ], ]
    }


def _atmos_orographic_gravity_wave_source_mechanisms():
    """Physical mechanisms generatic orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear mountain waves', None),
            ('hydraulic jump', None),
            ('envelope orography', None),
            ('statistical sub-grid scale variance', None),
            ('other', None),
        ]
    }


def _atmos_orographic_gravity_wave_calculation_method():
    """Calculation method for orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('non-linear calculation', None),
            ('more than two cardinal directions', None),
        ]
    }


def _atmos_orographic_gravity_wave_propagation_scheme():
    """Type of propagation scheme for orgraphic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear theory', None),
            ('non-linear theory', None),
            ('other', None),
        ]
    }       


def _atmos_orographic_gravity_wave_dissipation_scheme():
    """Type of dissipation scheme for orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('total wave', None),
            ('single wave', None),
            ('spectral', None),
            ('linear', None), 
            ('other', None),
        ]
    }

                
# Algorithm: Atmosphere: gravity waves: non-orographic gravity waves
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: source mechanisms
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: calculation method
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: propagation scheme
# Process-detail: Atmosphere: gravity waves: non-orographic gravity waves: dissipation scheme
def _atmos_non_orographic_gravity_waves():
    """Gravity waves generated by non-orographic processes."""
    return {
        'type': 'class',
        'vocab_status': 'initial',
        'base': 'science.algorithm',
        'properties': [
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_source_mechanisms',
                '1.N', 'Non-orographic gravity wave source mechanisms'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_calculation_method',
                '1.N', 'Non-orographic gravity wave calculation method'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_propagation_scheme',
                '1.1', 'Non-orographic gravity wave propogation scheme'),
            ('selection', 'cmip6.atmos_non_orographic_gravity_waves_dissipation_scheme',
                '1.1', 'Non-orographic gravity wave dissipation scheme'), ],
        'constraints': [
            [('heading', 'value', 'Non-orographic gravity wave source mechanisms'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_source_mechanisms.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_source_mechanisms', '1.N'), ],
            [('heading', 'value', 'Non-orographic gravity wave calculation method'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_calculation_method.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_calculation_method', '1.N'), ],
            [('heading', 'value', 'Non-orographic gravity wave propagation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_propogation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_propogation_scheme', '1.1'), ],
            [('heading', 'value', 'Non-orographic gravity wave dissipation scheme'),
                ('vocabulary', 'value', 'cmip6.atmos_non_orographic_gravity_wave_dissipation_scheme.%s' % version),
                ('selection', 'from', 'cmip6.atmos_non_orographic_gravity_wave_dissipation_scheme', '1.1'), ], ]
    }


def _atmos_non_orographic_gravity_wave_source_mechanisms():
    """Physical mechanisms generating non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('convection', None),
            ('precipitation', None),
            ('background spectrum', None),
            ('other', None),
        ]
    }


def _atmos_non_orographic_gravity_wave_calculation_method():
    """Calculation method for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('spatially dependent', None),
            ('temporally dependent', None),
            ]
    }


def _atmos_non_orographic_gravity_wave_propagation_scheme():
    """Type of propagation scheme for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('linear theory', None),
            ('non-linear theory', None),
            ('other', None),
        ]
    }       


def _atmos_non_orographic_gravity_wave_dissipation_scheme():
    """Type of dissipation scheme for non-orographic gravity waves."""
    return {
        'type': 'enum',
        'vocab_status': 'initial',
        'base': None,
        'is_open': False,
        'members': [
            ('total wave', None),
            ('single wave', None),
            ('spectral', None),
            ('linear', None), 
            ('other', None),
        ]
    }
