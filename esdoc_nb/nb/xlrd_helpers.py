import xlrd

__author__ = 'BNL28'


class Sheet(object):

    """ This is a sheet in an excel workbook which conforms to the metamodel for cim notebook spreadsheets.
    For each sheet, of a particular cim type, the column names correspond to the attributes of
    that particular class. Where there are multiple values of the attribute, if it is a document
    class, the column is repeated. If it is an encapsulated class additional rows appear. An
    example document is available.
    """

    def __init__(self, xlsheet):
        """ Instantiate with a sheet object returned by xlrd """
        self.sheet = xlsheet

        # Columns are not directly one-to-one with attributes
        # For example, one attribute may have more than property itself, so it may be
        #              spread over multiple columns. We get a list of attributes and
        #              properties as follows:
        # self.attributes is the list of attribute index collections.

        self.base_columns = [x.value for x in self.sheet.row(0)]
        self.attr_indexes = []
        for i, c in enumerate(self.base_columns):
            if c == '':
                self.base_columns[i] = self.base_columns[i-1]
                self.attr_indexes[-1].append(i)
            else:
                self.attr_indexes.append([i, ])

        if len(self.sheet.cell_value(1, 0)) == 0:
            self.column_attributes = [x.value for x in self.sheet.row(1)]
            for i, c in enumerate(self.column_attributes):
                if c == '':
                    if self.base_columns[i] == self.base_columns[i-1]:
                        self.column_attributes[i] = self.column_attributes[i-1]
            self.first_data_row = 2
        else:
            self.column_attributes = ['' for i in self.base_columns]
            self.first_data_row = 1

        self.columns = [(self.base_columns[i], self.column_attributes[i]) for i in range(len(self.base_columns))]

        self.rows = list(enumerate(self.sheet.get_rows()))

        self.row_indexes = self._get_row_indices()

    def get_attribute_names(self):
        """ Loop over the attribute columns"""
        for a_list in self.attr_indexes:
            yield [self.columns[i] for i in a_list]

    def get_attributes_from_row(self, row):
        """ Return attributes from a row"""
        for a_list in self.attr_indexes:
            yield [row[i].value for i in a_list]

    def get_attributes(self, row):
        """ Return all non-empty attributes in row """
        def _empty_cell(x):
            if x.ctype == 0:
                return 1
            elif x.ctype == 1 and x.value =='':
                return 1
            return 0
        for a_list in self.attr_indexes:
            r = []
            for i in a_list:
                # need this test for the case where the merged cell column heading goes beyond last data column
                if i < len(row):
                    cell = row[i]
                    if not _empty_cell(cell):
                        r.append(self.columns[i]+(cell.value,))
            yield r

    def get_data_rows(self):
        """ Return data rows as a list of lists of attribute names and values, where necessary
        including attributes with sub-properties as tuples within list elements. """
        for indices in self._get_row_indices():
            r = []
            for i in indices:
                for a in self.get_attributes(self.sheet.row(i)):
                    if a:
                        r.append(a)
            yield r

    def _get_row_indices(self):
        """ Find the primary data rows (the ones which do not start with blank cells)
        and create a list of them and any supplementary rows"""
        row_indices = []
        for idx, row in self.rows[self.first_data_row:]:
            if len(row[0].value) > 0:
                row_indices.append([idx,])
            else:
                row_indices[-1].append(idx)
        return row_indices

    def _get_row_by_index(self,index):
        """ Utility method to get a specific row by index"""
        return self.rows[index]

    def _get_cell_by_index(self, ri, ci):
        """ Utilty method to get a specific cell by index """
        return self.sheet.cell_value(ri,ci)

class WorkBook(object):

    """ Helper class for manipulating workbooks, based on Mark Morgan's utilities"""

    def __init__(self, workbook_filepath):

        """ Instantiate by reading file on disk
        :param workbook_filepath: Location of workbook on disk
        :return: self with sheet maps loaded as keys to base_columns in the attribute dictonary .sheets
        """

        self.book = xlrd.open_workbook(workbook_filepath)

        self.sheets = self.book.sheet_names()


    def get_by_name(self, ws_name):
        """ Return a specific
        :param ws_name: A worksheet name in the work book
        :return:
        """
        return Sheet(self.book.sheet_by_name(ws_name))